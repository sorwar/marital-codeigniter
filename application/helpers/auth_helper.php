<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/* Back End */

if ( ! function_exists('loginUser'))
{
	function loginUser()
	{
		$_CI = &get_instance();
		
		$adminUsername = $_CI->input->post('adminUsername');
        $adminPassword = $_CI->input->post('adminPassword');

         
        $userinfo = $_CI->M_crud->find('admin', array('adminUsername' => $adminUsername));
        // echo '<pre>';
        // print_r($userinfo); exit();


        if( !empty($userinfo) && $userinfo->adminPassword == md5($adminPassword) ) {

            setUserData($adminUsername);
            redirect("admin/dashboard");

        } else {
            redirect("admin");
        }
	}
}



if ( ! function_exists('setUserData'))
{
	function setUserData($adminUsername)
	{
		$_CI = &get_instance();
		$userData	= array(
			'adminUsername'    => $adminUsername
		);
		$_CI->session->set_userdata($userData);
	}
}


if ( ! function_exists('isActiveUser'))
{
	function isActiveUser()
	{
		$_CI = &get_instance();
		$adminUsername = $_CI->session->userData('adminUsername');
		
		if( $adminUsername == NULL ) {
			redirect("admin");
		}
		return true;
	}
}


if ( ! function_exists('logoutUser'))
{
	function logoutUser()
	{
		setUserData(NULL);
		
		if(!isActiveUser()){
			redirect("admin");
		}
	}
}

/* Front End */

if ( ! function_exists('frontLoginUser'))
{
	function frontLoginUser()
	{
		$_CI = &get_instance();
		
		$memUname = $_CI->input->post('memUname');
        $memPass  = $_CI->input->post('memPass');

        $userinfo = $_CI->M_crud->find('members', array('memUname' => $memUname));

        if( !empty($userinfo) && $userinfo->memPass == md5($memPass) && $userinfo->status == '1' ) {

            frontSetUserData($memUname);
            redirect("home/index");

        } elseif ($userinfo->status == '0') {
        	redirect("home/login/"."deactivated");
        } elseif ($userinfo->status == '2') {
        	redirect("home/login/"."not-active");
        } else {
            redirect("home/login/"."failed");
        }
	}
}



if ( ! function_exists('frontSetUserData'))
{
	function frontSetUserData($memUname)
	{
		$_CI = &get_instance();
		$userData	= array(
			'memUname'    => $memUname
		);
		$_CI->session->set_userdata($userData);
	}
}


if ( ! function_exists('frontIsActiveUser'))
{
	function frontIsActiveUser()
	{
		$_CI = &get_instance();
		$memUname = $_CI->session->userData('memUname');
		
		if( $memUname == NULL ) {
			redirect("home/login/");
		}
		return true;
	}
}


if ( ! function_exists('frontLogoutUser'))
{
	function frontLogoutUser()
	{
		frontSetUserData(NULL);
		
		if(!frontIsActiveUser()){
			redirect("home/login");
		}
	}
}



// ------------------------------------------------------------------------
/* End of file authentication_helper.php */
/* Location: ./application/helpers/authentication_helper.php */