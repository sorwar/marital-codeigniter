<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller{

    static $model = array();
    static $helper = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->model(self::$model);
        $this->load->helper(self::$helper);
    }

    public function index()
    {
        $data['basicInfo'] = $this->M_crud->find('basic', array());
        $data['memberInfo'] = $this->M_crud->findAll('members', array(),'memId desc', 0, 20);

        $findVisitor = $this->M_crud->find('visitor_count', array());

        if (!empty($findVisitor)) {
            $pata['visitorCount'] = $findVisitor->visitorCount + 1;
            $this->M_crud->update('visitor_count', $pata, array('visitorId' => $findVisitor->visitorId));
        } else {
            $pata['visitorCount'] = '1';
            $this->M_crud->save('visitor_count', $pata);
        }

        $this->load->view('marital/index', $data);
    }

    public function register()
    {
        $data['basicInfo'] = $this->M_crud->find('basic', array());
        $this->load->view('marital/register', $data);
    }

    public function registerAction()
    {
        /*echo '<pre>';
        print_r($_POST);*/

        $data['memUname']       = $this->input->post('memUname');
        $data['memPass']        = md5($this->input->post('memPass'));
        $data['memEmail']       = $this->input->post('memEmail');
        $data['status']         = '2';
        $data['memAgeDate']     = $this->input->post('memAgeDate');
        $data['memAgeMonth']    = $this->input->post('memAgeMonth');
        $data['memAgeYear']     = $this->input->post('memAgeYear');
        $data['memSex']         = $this->input->post('memSex');

        if ($data['memSex'] == 'Male'):
            $data['memMSex'] = 'Groom';
        elseif ($data['memSex'] == 'Female'):
            $data['memMSex'] = 'Bride';
        endif;

        $data['memMarital']     = $this->input->post('memMarital');

        $current    = date('d/m/Y');
        $expl       = explode('/', $current);

        $currYear   = $expl['2'];
        $currMonth  = $expl['1'];

        $userYear   = $this->input->post('memAgeYear');
        $dbMonth    = $this->input->post('memAgeMonth');

        if ($dbMonth == 'January'):
            $userMonth = '1';
        elseif ($dbMonth == 'February'):
            $userMonth = '2';
        elseif ($dbMonth == 'March'):
            $userMonth = '3';
        elseif ($dbMonth == 'April'):
            $userMonth = '4';
        elseif ($dbMonth == 'May'):
            $userMonth = '5';
        elseif ($dbMonth == 'June'):
            $userMonth = '6';
        elseif ($dbMonth == 'July'):
            $userMonth = '7';
        elseif ($dbMonth == 'August'):
            $userMonth = '8';
        elseif ($dbMonth == 'September'):
            $userMonth = '9';
        elseif ($dbMonth == 'October'):
            $userMonth = '10';
        elseif ($dbMonth == 'November'):
            $userMonth = '11';
        elseif ($dbMonth == 'December'):
            $userMonth = '12';
        endif;

        $birthMonth = 12 - $userMonth;

        $mainYear   = $currYear - $userYear + 1 - 2;

        $tempAge    = $mainYear * 12;
        $tempMonth  = $tempAge + $currMonth + $birthMonth;
        $originalAge = $tempMonth / 12;

        $explYear = explode('.', $originalAge);
        $data['memAge'] = $explYear['0'];

        $find = $this->M_crud->find('members', array('memUname' => $data['memUname']));
        $name = $find->memUname;

        if ($data['memUname'] == $name) {
            redirect('home/register/'.'did_match');
        } else {
            $find = $this->M_crud->find('members', array(), 'memId desc');
            $memProId = $find->memProId;

            if (!empty($memProId)):
                    $explode = explode('-', $memProId);
                    $plusOne = $explode[1] + 1;
                    $newMemProId = $explode[0].'-00'.$plusOne;
                    $data['memProId'] = $newMemProId;
            else: 
                    $data['memProId'] = 'MRS-001';
            endif;

            $save = $this->M_crud->save('members', $data);
            redirect('home/register/'.'registered');
        }

    }

    public function login()
    {
        $data['basicInfo'] = $this->M_crud->find('basic', array());
        $this->load->view('marital/login', $data);
    }

    public function loginAction()
    {
        frontLoginUser();
    }

    public function logout()
    {
        frontLogoutUser();
    }

    public function menu($id)
    {
        $data['basicInfo'] = $this->M_crud->find('basic', array());
        $data['menu'] = $this->M_crud->find('menu', array('menuStatus' => '1', 'menuId' => $id));

        $this->load->view('marital/menu', $data);
    }

    public function searchAction() 
    {

        $data['basicInfo']  = $this->M_crud->find('basic', array());
        $data['similler']   = $this->M_crud->findAll('members', array());
        $data['suggest']    = $this->M_crud->findAll('members', array(),'memId desc', 0, 4);

        $memMSex        = $this->input->post('memSex');
        $memBirthPlace  = $this->input->post('memBirthPlace');
        $memHeight      = $this->input->post('memHeight');
        $memMarital     = $this->input->post('memMarital');
        $fromAge        = $this->input->post('fromAge');
        $toAge          = $this->input->post('toAge');

        $condition = "(members.memMSex LIKE '%$memMSex%' && members.memBirthPlace LIKE '%$memBirthPlace%' && members.memHeight LIKE '%$memHeight%' && members.memMarital LIKE '%$memMarital%' && members.memAge > '$fromAge' && members.memAge < '$toAge')";

        $data['members'] = $this->M_crud->findAll('members', $condition, 0);

        $this->load->view('marital/search', $data);


    }

    

    
}

?>