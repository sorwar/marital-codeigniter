<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller{

    static $model = array();
    static $helper = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->model(self::$model);
        $this->load->helper(self::$helper);
        $this->load->library('upload');
    }

    public function index()
    {
        isActiveUser();

        $data['pageTitle']  = 'Marital';
        $data['message']    =  $this->Custom_model->adminMemJoin();
        $data['members']    =  $this->M_crud->findAll('members', array(),  'memId desc', 0, 0 );

        $data['activeMember']      =  $this->M_crud->findAll('members', array('status' => '1'),  'memId desc', 0, 0 );
        $data['pendingMember']     =  $this->M_crud->findAll('members', array('status' => '2'),  'memId desc', 0, 0 );
        $data['inactiveMember']    =  $this->M_crud->findAll('members', array('status' => '0'),  'memId desc', 0, 0 );
        $data['allMessages']       =  $this->M_crud->findAll('messages', array(),  'msgId desc', 0, 0 );
        $data['allMenu']           =  $this->M_crud->findAll('menu', array());

        $data['findVisitor'] = $this->M_crud->find('visitor_count', array());

        $data['page'] = 'admin/dashboard';

       
        
        $this->load->view('mainPage', $data);

    }

    public function inactiveMembers()
    {
        isActiveUser();

        $data['pageTitle']         = 'Members';
        $data['page']              = 'admin/members';

        $data['members']           = $this->M_crud->findAll('members', array('status' => '2'), 'memId desc', 0, 0);
        $this->load->view('mainPage', $data);
    }

    public function activeMembers()
    {
        isActiveUser();

        $data['pageTitle']          = 'Members';
        $data['page']               = 'admin/members';

        $data['members']            = $this->M_crud->findAll('members', array('status !=' => '2'), 'memId desc', 0, 0);
        $this->load->view('mainPage', $data);
    }

    public function memMsg()
    {
        isActiveUser();
        $currentUrl                = $this->input->post('currentUrl');
        $memId                     = $this->input->post('memId');
        $data['msgReceiver']       = $this->input->post('recieverId');
        $data['msgBox']            = $this->input->post('msgBox');
        $data['msgAdmin']          = 'Yes';

        $pata['status']            = $this->input->post('status');

        $this->M_crud->update('members', $pata, array('memId' => $memId));
        if (!empty($data['msgBox'])):
            $this->M_crud->save('messages', $data);
        endif;
         
        redirect($currentUrl);
    }

     public function deleteAction($memId)
    {
        isActiveUser();
        $this->M_crud->destroy('members', array('memId' => $memId));

        redirect($_SERVER['HTTP_REFERER']);

    }

    public function menu()
    {
        isActiveUser();
        $data['pageTitle']        = 'menu';
        $data['page']             = 'admin/menu';
        $data['menu']             = $this->M_crud->findAll('menu', array());
        $this->load->view('mainPage', $data);
    }

    public function addMenu()
    {
        isActiveUser();
        $data['menuId   ']       = $this->input->post('menuId');
        $data['menuName ']       = $this->input->post('menuName');
        $data['menuLink ']       = $this->input->post('menuLink');
        $data['menuTitle']       = $this->input->post('menuTitle');
        $data['menuDis  ']       = $this->input->post('menuDis');
         
        $this->M_crud->save('menu', $data, array());
        
        

        redirect($_SERVER['HTTP_REFERER']);

    } 

     public function updateMenu()
    { 
        isActiveUser();
        $currentUrl              = $this->input->post('currentUrl');
        $menuId                  = $this->input->post('menuId');
        $data['menuName']        = $this->input->post('menuName');
        $data['menuLink']        = $this->input->post('menuLink');
        $data['menuTitle']       = $this->input->post('menuTitle');
        $data['menuDis']         = $this->input->post('menuDis');
        $data['menuStatus']      = $this->input->post('status');

        $this->M_crud->update('menu', $data, array('menuId' => $menuId));

        redirect($currentUrl);

    }

    public function contact()
    {   
        isActiveUser();
        $data['pageTitle']      = 'contact';
        $data['page']           = 'admin/contact';

        $data['contact']        = $this->M_crud->find('contact', array());          

        $this->load->view('mainPage', $data);
    }

    public function updateContact()
    { 
        isActiveUser();

        $currentUrl              = $this->input->post('currentUrl');
        $menuId                  = $this->input->post('menuId');
        $data['menuName']        = $this->input->post('menuName');
        $data['menuLink']        = $this->input->post('menuLink');
        $data['menuTitle']       = $this->input->post('menuTitle');
        $data['menuDis']         = $this->input->post('menuDis');
        $data['menuStatus']      = $this->input->post('status');

        $this->M_crud->update('contact', $data, array('menuId' => $menuId));

        redirect($currentUrl);

    }


    public function basic()
    {
        isActiveUser();
        $data['pageTitle']         = 'basic';
        $data['page']              = 'admin/basic';

         $data['basic']        = $this->M_crud->find('basic', array());       

        $this->load->view('mainPage', $data);
    }

     public function updateBasic()
    { 
        //  echo '<pre>';
        // print_r($_POST); exit();
        isActiveUser();

        $currentUrl              = $this->input->post('currentUrl');
        $id                      = $this->input->post('id');
        $data['about']           = $this->input->post('about');
        $data['map']             = $this->input->post('map');
        $data['facebook']        = $this->input->post('facebook');                                                
        $data['twitter']         = $this->input->post('twitter');
        $data['google']          = $this->input->post('google');
        $data['youtube']         = $this->input->post('youtube');
        $data['headTitle']       = $this->input->post('headTitle');
        $data['footerTitle']     = $this->input->post('footerTitle');

        $agerImage = $this->M_crud->find('basic', array('id' => $id));
        $img_field = array('logo', 'background');
        foreach ($img_field as $v) {
            
            $config['upload_path']      = './uploads/';
            $config['allowed_types']    = 'gif|jpg|png|jpeg';
            $config['file_name']        = time();
            $this->upload->initialize($config);

            if ($this->upload->do_upload($v)) {

                $upload_data = $this->upload->data();
                $data[$v]    = $upload_data['file_name'];

                if(!empty($agerImage->$v) && file_exists('./uploads/'.$agerImage->$v)){
                    unlink('./uploads/'.$agerImage->$v);
                }

            }

        }


        $this->M_crud->update('basic', $data, array('id' => $id));

        redirect($currentUrl);

    }

    public function login()
    {
        $this->load->view('login');
    }
    
    public function loginAction()
    {
        loginUser();
    }

    public function logout()
    {
        logoutUser();
    }

    public function files()
    {
         isActiveUser();

        $data['pageTitle']         = 'files';
        $data['page']              = 'admin/files';

         $data['basic']        = $this->M_crud->find('basic', array());       

        $this->load->view('mainPage',$data);
    }
}


?>