<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Members extends CI_Controller{

    static $model = array();
    static $helper = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->model(self::$model);
        $this->load->helper(self::$helper);
        $this->load->library('upload');
    }

    public function index()
    {
        $data['basicInfo']  = $this->M_crud->find('basic', array());
        $data['similler']   = $this->M_crud->findAll('members', array());
        $data['suggest']    = $this->M_crud->findAll('members', array(),'memId desc', 0, 4);
        $data['members']    = $this->M_crud->findAll('members', array(),'memId desc', 0);

        $this->load->view('marital/search', $data); 

    }

    public function viewProfile($memId)
    {
        $data['basicInfo']  = $this->M_crud->find('basic', array());
        $data['memInfo']    = $this->M_crud->find('members', array('status' => '1', 'memId' => $memId));

        if ($data['memInfo']->memSex == 'Female'):
            $data['similler']   = $this->M_crud->findAll('members', array('status' => '1', 'memAge' => $data['memInfo']->memAge, 'memSex' => 'Female', 'memId !=' => $memId), 'memId desc', 0, 4);
        elseif ($data['memInfo']->memSex == 'Male'):
            $data['similler']   = $this->M_crud->findAll('members', array('status' => '1', 'memAge' => $data['memInfo']->memAge, 'memSex' => 'Male', 'memId !=' => $memId), 'memId desc', 0, 4);
        endif;

        $memUname = $this->session->userData('memUname');

        if (!empty($memUname)):
            $userSex  = $this->M_crud->find('members', array('memUname' => $memUname));

            $data['suggest']  = $this->M_crud->findAll('members', array('status' => '1', 'memSex != ' => $userSex->memSex), 'memId desc', 0, 4); 

        else:
            $data['suggest']  = $this->M_crud->findAll('members', array('status' => '1'), 0, 4);
        endif;

        /*echo '<pre>';
        print_r($data['suggest']); exit();*/
        
        $this->load->view('marital/viewProfile', $data);
    }


    public function updateInfo($memId)
    {
        frontIsActiveUser();

        $bokkiChokki = $this->session->userData('memUname');
        $bokki       = $this->M_crud->find('members', array('memUname' => $bokkiChokki, 'status' => '1'));
        $chokki      = $bokki->memId;

        if ($memId == $chokki) {

            $data['memInfo']    = $this->M_crud->find('members', array('status' => '1', 'memId' => $memId));

            if ($data['memInfo']->memSex == 'Female'):
                $data['similler']   = $this->M_crud->findAll('members', array('status' => '1', 'memAge' => $data['memInfo']->memAge, 'memSex' => 'Female', 'memId !=' => $memId), 'memId desc', 0, 4);
            elseif ($data['memInfo']->memSex == 'Male'):
                $data['similler']   = $this->M_crud->findAll('members', array('status' => '1', 'memAge' => $data['memInfo']->memAge, 'memSex' => 'Male', 'memId !=' => $memId), 'memId desc', 0, 4);
            endif;

            $memUname = $this->session->userData('memUname');

            if (!empty($memUname)):
                $userSex  = $this->M_crud->find('members', array('memUname' => $memUname));

                $data['suggest']  = $this->M_crud->findAll('members', array('status' => '1', 'memSex != ' => $userSex->memSex), 'memId desc', 0, 4); 

            else:
                $data['suggest']  = $this->M_crud->findAll('members', array('status' => '1'), 0, 4);
            endif;

            $data['basicInfo']  = $this->M_crud->find('basic', array());
            $data['memInfo']    = $this->M_crud->find('members', array('status' => '1', 'memId' => $memId));

            $this->load->view('marital/updateInfo', $data);
        } else {
            echo '<h1 style="text-align: center; padding-top: 20%">Apni vul jaygay probes koresen!</h1>';
        }
        
    }

    public function adMemberInfo()
    {
        $memId                      = $this->input->post('memId');
        $data['memName']            = $this->input->post('memName');
        $data['memMarital']         = $this->input->post('memMarital');
        $data['memBody']            = $this->input->post('memBody');
        $data['memHeight']          = $this->input->post('memHeight');
        $data['memPhysical']        = $this->input->post('memPhysical');
        $data['memCreated']         = $this->input->post('memCreated');
        $data['memDrink']           = $this->input->post('memDrink');
        $data['memAge']             = $this->input->post('memAge');
        $data['memTongue']          = $this->input->post('memTongue');
        $data['memComplexion']      = $this->input->post('memComplexion');
        $data['memWeight']          = $this->input->post('memWeight');
        $data['memBlood']           = $this->input->post('memBlood');
        $data['memDiet']            = $this->input->post('memDiet');
        $data['memSmoke']           = $this->input->post('memSmoke');
        $data['memBirthTime']       = $this->input->post('memBirthTime');
        $data['memCaste']           = $this->input->post('memCaste');
        $data['memBirthPlace']      = $this->input->post('memBirthPlace');
        $data['memReligion']        = $this->input->post('memReligion');
        $data['memSubCaste']        = $this->input->post('memSubCaste');
        $data['memRaasi']           = $this->input->post('memRaasi');
        $data['memEdu']             = $this->input->post('memEdu');
        $data['memEduDetail']       = $this->input->post('memEduDetail');
        $data['memOccDetail']       = $this->input->post('memOccDetail');
        $data['memAnnualIncome']    = $this->input->post('memAnnualIncome');
        $data['memFaOcc']           = $this->input->post('memFaOcc');
        $data['memMaOcc']           = $this->input->post('memMaOcc');
        $data['memBroNum']          = $this->input->post('memBroNum');
        $data['memSisNum']          = $this->input->post('memSisNum');
        $data['memPartAge']         = $this->input->post('memPartAge');
        $data['memPartMarital']     = $this->input->post('memPartMarital');
        $data['memPartBody']        = $this->input->post('memPartBody');
        $data['memPartComplexion']  = $this->input->post('memPartComplexion');
        $data['memPartHeight']      = $this->input->post('memPartHeight');
        $data['memPartManglik']     = $this->input->post('memPartManglik');
        $data['memPartReligion']    = $this->input->post('memPartReligion');
        $data['memPartCaste']       = $this->input->post('memPartCaste');
        $data['memPartTongue']      = $this->input->post('memPartTongue');
        $data['memPartEducation']   = $this->input->post('memPartEducation');
        $data['memPartOccupation']  = $this->input->post('memPartOccupation');
        $data['memPartCountry']     = $this->input->post('memPartCountry');
        $data['memPartState']       = $this->input->post('memPartState');
        $data['memPartResidency']   = $this->input->post('memPartResidency');
        $data['memAgeDate']         = $this->input->post('memAgeDate');
        $data['memAgeMonth']        = $this->input->post('memAgeMonth');
        $data['memAgeYear']         = $this->input->post('memAgeYear');

        $agerImage = $this->M_crud->find('members', array('memId' => $memId));
        $img_field = array('memImage1', 'memImage2', 'memImage3', 'memImage4');
        foreach ($img_field as $v) {
            
            $config['upload_path']      = './uploads/';
            $config['allowed_types']    = 'gif|jpg|png|jpeg';
            $config['file_name']        = time();
            $this->upload->initialize($config);

            if ($this->upload->do_upload($v)) {

                $upload_data = $this->upload->data();
                $data[$v]    = $upload_data['file_name'];

                if(!empty($agerImage->$v) && file_exists('./uploads/'.$agerImage->$v)){
                    unlink('./uploads/'.$agerImage->$v);
                }

            }

        }

        $this->M_crud->update('members', $data, array('memId' => $memId));

        redirect('members/updateInfo/'.$memId);
    }

    public function messageAction()
    {
        // echo '<pre>';
        // print_r($_POST);
        // exit();
        $redirectUrl             = $this->input->post('redirectUrl');
        $data['msgBox']          = $this->input->post('msgBox');
        $data['msgSender']       = $this->input->post('senderId');
        $data['msgReceiver']     = $this->input->post('receiverId');

        $save = $this->M_crud->save('messages', $data);
        redirect($redirectUrl);

    }

    public function inbox()

    {  
        $data['basicInfo']  = $this->M_crud->find('basic', array());
        $data['similler']   = $this->M_crud->findAll('members', array());
        $data['suggest']    = $this->M_crud->findAll('members', array(),'memId desc', 0, 4);

        $memUname = $this->session->userData('memUname');
        $find     = $this->M_crud->find('members', array('memUname'=> $memUname));
        $recId    = $this->M_crud->find('messages', array('msgReceiver' => $find->memId));
        $sndId    = $this->M_crud->find('messages', array('msgSender' => $find->memId));

        if (!empty($recId)):
            $userId = $recId->msgReceiver;
        elseif (!empty($sndId)):
            $userId = $sndId->msgSender;
        else:
            $userId = NULL;
        endif;

        $data['inbox'] = $this->Custom_model->inboxJoin($userId);
        $data['sent']  = $this->Custom_model->sentJoin($userId);

        /*echo '<pre>';
        print_r($data['sent']);
        exit();*/

        $this->load->view('marital/inbox', $data);        
        

    }

    public function search()
    {
        $data['basicInfo']  = $this->M_crud->find('basic', array());
        $data['similler']   = $this->M_crud->findAll('members', array());
        $data['suggest']    = $this->M_crud->findAll('members', array(),'memId desc', 0, 4);
        
        $searchValue = $this->input->post('search');
        $condition = "(members.memProId LIKE '%$searchValue%' OR members.memUname LIKE '%$searchValue%' OR members.memSex LIKE '%$searchValue%' OR members.memAge LIKE '%$searchValue%')";

        $data['members'] = $this->M_crud->findAll('members', $condition, 0);

        $this->load->view('marital/search', $data);

    }

    
}

?>