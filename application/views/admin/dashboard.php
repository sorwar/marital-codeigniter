<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
		<div class="alert alert-block alert-success">
			<button type="button" class="close" data-dismiss="alert">
				<i class="ace-icon fa fa-times"></i>
			</button>

			<i class="ace-icon fa fa-check green"></i>

			Welcome to
			<strong class="green">
				marital
				<small>Admin pannel</small>
			</strong>,
		</div>

		<div class="row">
			<div class="space-6"></div>

				<div class="col-sm-7 infobox-container">
				<div class="infobox infobox-green">
					<div class="infobox-icon">
						<i class="ace-icon fa fa-check"></i>
					</div>

					<div class="infobox-data">
						<span class="infobox-data-number"><?php echo count($activeMember) ?></span>
						<div class="infobox-content">Active Members</div>
					</div>

					<div class="stat stat-success">8%</div>
				</div>

				<div class="infobox infobox-purple">
					<div class="infobox-icon">
						<i class="ace-icon fa fa-hourglass-2"></i>
					</div>

					<div class="infobox-data">
						<span class="infobox-data-number"><?php echo count($pendingMember) ?></span>
						<div class="infobox-content">Pending Members</div>
					</div>

					<div class="badge badge-success">
						+32%
						<i class="ace-icon fa fa-arrow-up"></i>
					</div>
				</div>

				<div class="infobox infobox-red">
					<div class="infobox-icon">
						<i class="ace-icon fa fa-ban"></i>
					</div>

					<div class="infobox-data">
						<span class="infobox-data-number"><?php echo count($inactiveMember) ?></span>
						<div class="infobox-content">Inactive Members</div>
					</div>
					<div class="stat stat-important">4%</div>
				</div>

				<div class="infobox infobox-pink">
					<div class="infobox-icon">
						<i class="ace-icon fa fa-comments"></i>
					</div>

					<div class="infobox-data">
						<span class="infobox-data-number"><?php echo count($allMessages) ?></span>
						<div class="infobox-content">All Messages</div>
					</div>
				</div>
				<div class="infobox infobox-black">
					<div class="infobox-icon">
						<i class="ace-icon fa fa-bar-chart"></i>
					</div>

					<div class="infobox-data">
						<span class="infobox-data-number"><?php echo ($findVisitor->visitorCount) ?></span>
						<div class="infobox-content">pageviews</div>
					</div>
				</div>

				<div class="infobox infobox-purple">
					<div class="infobox-icon">
						<i class="ace-icon fa fa-arrow-up"></i>
					</div>

					<div class="infobox-data">
						<span class="infobox-data-number"><?php echo count($allMenu) ?></span>
						<div class="infobox-content">Extra pages</div>
					</div>
				</div>

				<div class="space-6"></div>

				 <a href="<?php echo site_url('admin/dashboard/menu') ?>"><div class="infobox infobox-green infobox-small infobox-dark">
					<div class="infobox-icon">
					 <i class="ace-icon glyphicon glyphicon-indent-left"></i>
						<div class="easy-pie-chart" data-values="3,4,2,3,4,4,2,2">">
						</div>
					</div>

					<div class="infobox-data">
						<div class="infobox-content">Menu</div>
						<div class="infobox-content">dynamic</div>
					</div>
				</div> </a>

				<a href="<?php echo site_url('admin/dashboard/contact') ?>"><div class="infobox infobox-blue infobox-small infobox-dark">
					<div class="infobox-icon">
						<span class="sparkline" data-values="3,4,2,3,4,4,2,2"></span>
						<i class="ace-icon fa fa-address-card"></i>
					</div>

					<div class="infobox-data">
						<div class="infobox-content">Contact </div>
						<div class="infobox-content">information</div>
					</div>
				</div> </a>

				<a href="<?php echo site_url('admin/dashboard/basic') ?>"><div class="infobox infobox-grey infobox-small infobox-dark">
					<div class="infobox-icon">
						<i class="ace-icon fa fa-pencil-square-o"></i>
					</div>

					<div class="infobox-data">
						<div class="infobox-content">Basic</div>
						<div class="infobox-content">information</div>
					</div>
				</div> </a>
			</div>

			<div class="vspace-12-sm"></div>

			<div class="col-sm-5">
				<div class="widget-box">
					<div class="widget-header">
						<h4 class="widget-title lighter smaller">
							<i class="ace-icon fa fa-comment blue"></i>
							Conversation
						</h4>
					</div>

					<div class="widget-body">
						<div class="widget-main no-padding">
							<div class="dialogs" style="height: 180px; overflow: auto;">
							<?php foreach ($message as $v): ?>
								<div class="itemdiv dialogdiv">
									<div class="user">
									    <?php if (empty($v->senderImage)):?>

											   <img class="img-circle" height="40" src=" <?php echo base_url('resource/front-end/Images/default.jpg') ?>" >

											<?php else : ?>

												<img class="img-circle" height="40" alt="marital member" src="<?php echo base_url('uploads/'.$v->senderImage) ?>" />

											<?php endif;?>

										
									</div>

									<div class="body">
										<!-- <div class="time">
											<i class="ace-icon fa fa-clock-o"></i>
											<span class="green">4 sec</span>
										</div> -->

										<div class="name">
											<a target="blank" href="<?php echo site_url('members/viewProfile/'.$v->msgSender)?>"><?php echo $v->senderUname ?></a>
										
										
										<span class="name">
										to
											<a target="blank" href="<?php echo site_url('members/viewProfile/'.$v->msgReceiver)?>"><?php echo $v->receiverUname ?></a>
										</span>
										</div>
										<div class="text"><?php echo $v->msgBox ?></div>

										<div class="tools">
											<a href="#" class="btn btn-minier btn-info">
												<i class="icon-only ace-icon fa fa-share"></i>
											</a>
										</div>
									</div>
								</div>
							<?php endforeach ?>
							</div>
						</div><!-- /.widget-main -->
					</div><!-- /.widget-body -->
				</div><!-- /.widget-box -->
			</div><!-- /.col --><!-- /.col -->
		</div><!-- /.row -->

		<div class="hr hr32 hr-dotted"></div>


		<div class="row">
			<div class="col-sm-12">
				<div class="widget-box transparent" id="recent-box">
					<div class="widget-header">
						<h4 class="widget-title lighter smaller">
							<i class="ace-icon fa fa-rss orange"></i>RECENT
						</h4>

						<div class="widget-toolbar no-border">
							<ul class="nav nav-tabs" id="recent-tab">
								<li class="active">
									<a data-toggle="tab" href="#member-tab">Members</a>
								</li>
							</ul>
						</div>
					</div>

					<div class="widget-body">
						<div class="widget-main padding-4">
							<div class="tab-content padding-8">
								<div id="member-tab" class="tab-pane active">
									<div class="clearfix">
									<?php foreach ($members as $v) : ?>
										<div class="itemdiv memberdiv">
											<div class="user">
											<?php if (empty($v->memImage1)):?>

											   <img class="img-circle" height="60" src=" <?php echo base_url('resource/front-end/Images/default.jpg') ?>" >

											<?php else : ?>

												<img class="img-circle" height="60" alt="marital member" src="<?php echo base_url('uploads/'.$v->memImage1) ?>" />
											<?php endif;?>

											</div>

											<div class="body">
												<div class="name">
													<a href="#"><?php echo $v->memUname ?></a>
												</div>

												<div class="time">
													<i class=""></i>
													<span class="green"><?php echo $v->memAge?> yrs</span>
												</div>

												<div>
													<?php if($v->status == 1): ?>
														<span class="label label-success arrowed-in arrowed-in-right">Active</span>
														<i style="color: green" class="ace-icon fa fa-check-circle fa-1x icon-animated-bell"></i>

													<?php elseif ($v->status == 2): ?>

														<span class="label label-sm label-warning">Pending</span>

													<?php else: ?>
									            		<i style="color: red" class="ace-icon fa fa-check-circle fa-1x icon-animated-bell"></i>
														<span class="label label-danger arrowed-in">Inactive</span>

													<?php endif ?>
												</div>
											</div>
										</div>
									<?php endforeach ?>
									</div>

									<div class="space-4"></div>

									<div class="center">
										<i class="ace-icon fa fa-users fa-2x green middle"></i>

										&nbsp;
										<a href="#" class="btn btn-sm btn-white btn-info">
											See all members &nbsp;
											<i class="ace-icon fa fa-arrow-right"></i>
										</a>
									</div>

									<div class="hr hr-double hr8"></div>
								</div><!-- /.#member-tab -->
							</div>
						</div><!-- /.widget-main -->
					</div><!-- /.widget-body -->
				</div><!-- /.widget-box -->
			</div><!-- /.col -->
		</div><!-- /.row -->

		<!-- PAGE CONTENT ENDS -->
	</div><!-- /.col -->
</div><!-- /.row -->