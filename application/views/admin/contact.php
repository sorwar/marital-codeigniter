
<table id="simple-table" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th class="center">
				<label class="pos-rel">
					<input type="checkbox" class="ace" />
					<span class="lbl"></span>
				</label>
			</th>
			<th>SL</th>
			<!-- <th class="text-center">Profile</th> -->
			<th>Address</th>
			<th>Phone</th>
			<th>Email</th>
			<th>Discription</th>
			<th class="hidden-480">Status</th>
			<th class="text-center">Action</th>
		</tr>
	</thead>

	<tbody>
			<tr>
				<td class="center">
					<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>
				</td>
				<td><?php echo '1' ?></td>
				<td style="width: 10%"><?php echo $contact->menuName ?></td>
				<td><?php echo $contact->menuLink ?></td>
				<td style="width: 10%"><?php echo $contact->menuTitle ?></td>
				<td><?php echo $contact->menuDis ?></td>
				<td class="hidden-480" style="width: 10%">
				<?php if($contact->menuStatus == 1): ?>
					<span class="label label-success arrowed-in arrowed-in-right">Active</span>
					<i style="color: green" class="ace-icon fa fa-check-circle fa-1x icon-animated-bell"></i>

				<?php elseif ($contact->menuStatus == 2): ?>

					<span class="label label-sm label-warning">Pending</span>

				<?php else: ?>
            		<i style="color: red" class="ace-icon fa fa-check-circle fa-1x icon-animated-bell"></i>
					<span class="label label-danger arrowed-in">Inactive</span>

	
				<?php endif ?>
				</td>
				<td>
					<div class="hidden-sm hidden-xs action-buttons">
					    <a class="btn btn-xs btn-success" id="show-details-btn">
					    	<i class="ace-icon fa fa-pencil-square-o bigger-130"></i>
					    </a>	
					</div>
				</td>
			</tr>
			<tr class="detail-row">
				<td colspan="10">
					<div class="table-detail">
						<div class="row">
							<form method="post" action="<?php echo site_url('admin/dashboard/updateContact') ?>">
								<input type="hidden" name="currentUrl" value="<?php echo current_url() ?>">
								<input type="hidden" name="menuId" value="<?php echo $contact->menuId ?>">

					            <div class="col-sm-6">
									<div class="form-group">
										<label class="col-sm-4 control-label no-padding-right" for="menuName"> Address : </label>
										<div class="col-sm-8">
											<input type="text" id="menuName" name="menuName" value="<?php echo $contact->menuName ?>" class="form-control" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label no-padding-right" for="menuName">  Phone Number : </label>
										<div class="col-sm-8">
											<input type="text" id="menuLink" name="menuLink" value="<?php echo $contact->menuLink ?>" class="form-control" />
										</div>
									</div>
								</div>

								<div class="col-sm-6">
									<div class="form-group">
										<label class="col-sm-4 control-label no-padding-right" for="menuName"> Email :</label>
										<div class="col-sm-8">
											<input type="text" id="menuName" name="menuTitle" value="<?php echo $contact->menuTitle ?>" class="form-control" />
										</div>
									</div>
									<!-- <div class="form-group">
										<label class="col-sm-4 control-label no-padding-right" for="menuName">  Menu Name </label>
										<div class="col-sm-8">
											<input type="text" id="menuName" name="menuName" value="<?php echo $contact->menuName ?>" class="form-control" />
										</div>
									</div> -->
								</div>

								<div class="clearfix"></div>
								<div class="space-4"></div>

					            <div class="col-sm-12">
									<div class="form-group">
										<div class="col-sm-12">
											<textarea name="menuDis" class="text-editor" id="Description "> <?php echo $contact->menuDis ?> </textarea>
										</div>
									</div>
								</div>
								
								<div class="clearfix"></div>
								<div class="space-4"></div>
								<div class="hr hr-dotted"></div>

								<div class="col-sm-12">
									<div class="row">
										<div class="col-sm-8">
											<div class="clearfix pull-right">
												<label>
													<input class="ace" name="status" value="1" <?php if ($contact->menuStatus == 1): echo 'checked="checked"'; endif ?> type="radio">
													<span class="lbl"> Active</span>
												</label>
												<label>
													<input class="ace" name="status" value="0" <?php if ($contact->menuStatus == 0): echo 'checked="checked"'; endif ?> type="radio">
													<span class="lbl"> Inactive</span>
												</label>
											</div>
										</div>
										<div class="col-sm-4">
											<button class="pull-right btn btn-sm btn-primary btn-white btn-round" type="submit">
												Submit
												<i class="ace-icon fa fa-arrow-right icon-on-right bigger-110"></i>
											</button>
										</div>
									</div>
								</div>
					             
								
						    </form>	
						</div>
					</div>
				</td>
			</tr> 
	</tbody>
</table>

<script src="<?php echo base_url('resource/back-end/js/jquery-2.1.4.min.js') ?>"></script>
	<script type="text/javascript">
		jQuery(function($) {
			$('#show-details-btn').on('click', function(e) {
				e.preventDefault();
				$(this).closest('tr').next().toggleClass('open');
				/*$(this).find(ace.vars['.icon']).toggleClass('fa-angle-double-down').toggleClass('fa-angle-double-up');*/
			});
		});
	</script>

