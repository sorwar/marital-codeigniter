<table id="simple-table" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th class="center">
				<label class="pos-rel">
					<input type="checkbox" class="ace" />
					<span class="lbl"></span>
				</label>
			</th>
			<th>SL</th>
			<th class="text-center">Profile</th>
			<th>Username</th>
			<th>Profile Id</th>
			<th>Sex</th>
			<th>Email</th>
			<th class="hidden-480">Status</th>
			<th>Action</th>
		</tr>
	</thead>

	<tbody>

		<?php $i = 1; $k = 1; $l = 1; foreach ($members as $v): ?>

			<tr>
				<td class="center">
					<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>
				</td>
				<td><?php echo $k++ ?></td>
				<td class="text-center">
					<button class="btn btn-xs btn-success" id="show-details-btn<?php echo $i++ ?>">View Profile</button>
				</td>
				<td><?php echo $v->memUname ?></td>
				<td><?php echo $v->memProId ?></td>
				<td><?php echo $v->memSex ?></td>
				<td><?php echo $v->memEmail ?></td>
				<td class="hidden-480">
				<?php if($v->status == 1): ?>
					<span class="label label-success arrowed-in arrowed-in-right">Active</span>
					<i style="color: green" class="ace-icon fa fa-check-circle fa-1x icon-animated-bell"></i>

				<?php elseif ($v->status == 2): ?>

					<span class="label label-sm label-warning">Pending</span>

				<?php else: ?>
            		<i style="color: red" class="ace-icon fa fa-check-circle fa-1x icon-animated-bell"></i>
					<span class="label label-danger arrowed-in">Inactive</span>

	
				<?php endif ?>
				</td>
				<td>
					<div class="hidden-sm hidden-xs action-buttons">
						<a class="btn btn-danger btn-xs red" href="#modal-table<?php echo $v->memId?>" data-toggle="modal">
						
							<i class="ace-icon fa fa-trash-o bigger-130"></i>
						</a>
					</div>
				</td>
			</tr>
			<tr class="detail-row">
				<td colspan="10">
					<div class="table-detail">
						<div class="row">
							<div class="col-xs-12 col-sm-2">
								<div class="text-center">
									<img height="150" width="130" class="thumbnail inline no-margin-bottom" alt="Domain Owner's Avatar" src="<?php echo base_url('uploads/'.$v->memImage1) ?>" />
									<br />
									<div class="width-80 label label-info label-xlg arrowed-in arrowed-in-right">
										<div class="inline position-relative">
											<a class="user-title-label" href="#">
												<span class="white"><?php echo $v->memName ?></span>
											</a>
										</div>
									</div>
								</div>
							</div>

							<div class="col-xs-12 col-sm-7">
								<div class="space visible-xs"></div>

								<div class="profile-user-info profile-user-info-striped">
									<div class="profile-info-row">
										<div class="profile-info-name"> Username </div>

										<div class="profile-info-value">
											<span><?php echo $v->memUname ?></span>
										</div>
									</div>

									<div class="profile-info-row">
										<div class="profile-info-name"> Location </div>

										<div class="profile-info-value">
											<i class="fa fa-map-marker light-orange bigger-110"></i>
											<span><?php echo $v->memBirthPlace ?></span>
										</div>
									</div>

									<div class="profile-info-row">
										<div class="profile-info-name"> Age </div>

										<div class="profile-info-value">
											<span><?php echo $v->memAge ?></span>
										</div>
									</div>

									<div class="profile-info-row">
										<div class="profile-info-name"> Birth Date </div>

										<div class="profile-info-value">
											<span><?php echo $v->memAgeDate.' - '.$v->memAgeMonth.' - '.$v->memAgeYear ?></span>
										</div>
									</div>

									<div class="profile-info-row">
										<div class="profile-info-name"> Marital Status </div>

										<div class="profile-info-value">
											<span><?php echo $v->memMarital ?></span>
										</div>
									</div>

									<div class="profile-info-row">
										<div class="profile-info-name"> About Me </div>

										<div class="profile-info-value">
											<span></span>
										</div>
									</div>
								</div>
							</div>


							<div class="col-xs-12 col-sm-3">
								<h4 class="header blue lighter less-margin">Send a message to <?php echo $v->memName ?></h4>

								<div class="space-6"></div>

								<form method="post" action="<?php echo site_url('admin/dashboard/memMsg') ?>">
									<input type="hidden" name="currentUrl" value="<?php echo current_url() ?>">
									<input type="hidden" name="recieverId" value="<?php echo $v->memId ?>">
									<input type="hidden" name="memId" value="<?php echo $v->memId ?>">
									<fieldset>
										<textarea class="width-100" name="msgBox" resize="none" placeholder="Type something…"></textarea>
									</fieldset>

									<div class="hr hr-dotted"></div>

									<div class="clearfix">
										
										<label>
											<input class="ace" name="status" value="1" <?php if ($v->status == 1): echo 'checked="checked"'; endif ?> type="radio">
											<span class="lbl"> Active</span>
										</label>
										<label>
											<input class="ace" name="status" value="0" <?php if ($v->status == 0): echo 'checked="checked"'; endif ?> type="radio">
											<span class="lbl"> Inactive</span>
										</label>
																				

										<button class="pull-right btn btn-sm btn-primary btn-white btn-round" type="submit">
											Submit
											<i class="ace-icon fa fa-arrow-right icon-on-right bigger-110"></i>
										</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</td>
			</tr>
			<div id="modal-table<?php echo $v->memId?>" class="modal fade" tabindex="-1">
				<div class="modal-dialog" style="width: 400px">
					<div class="modal-content">
						<div class="modal-header no-padding">
							<div class="table-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									<span class="white">&times;</span>
								</button>
								Are you sure?
							</div>
							<div class="modal-footer">
					            <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Cancel</button>
					             <a href="<?php echo site_url('admin/dashboard/deleteAction/'.$v->memId)?>" class="btn btn-sm btn-primary" type="button"><i class="fa fa-check"></i> Delete</a>
					        </div>
						</div>

						<div class="modal-body no-padding">
							
						</div>
					</div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			</div>
			

		<?php endforeach ?>
    
	</tbody>
</table>

<script src="<?php echo base_url('resource/back-end/js/jquery-2.1.4.min.js') ?>"></script>
<?php $i = 1; foreach ($members as $v): ?>
	<script type="text/javascript">
		jQuery(function($) {
			$('#show-details-btn<?php echo $i++?>').on('click', function(e) {
				e.preventDefault();
				$(this).closest('tr').next().toggleClass('open');
				/*$(this).find(ace.vars['.icon']).toggleClass('fa-angle-double-down').toggleClass('fa-angle-double-up');*/
			});
		});
	</script>
<?php endforeach ?>
