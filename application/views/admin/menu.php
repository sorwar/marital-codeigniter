<div class="page-header">
	<h1>
		Menu
		<small class="pink">
			<i class="ace-icon fa fa-hand-o-right icon-animated-hand-pointer blue"></i>
			<a href="#small" role="button" class="green" data-toggle="modal"> Add New Dynamic Menu</a>
		</small>
	</h1>
</div>
 <!-- Small Modal -->
<div class="modal fade" id="small" tabindex="-1" role="dialog" aria-hidden="true">
 <div class="modal-dialog modal-dialog-fromright">
    <div class="modal-content">
       <div class="modal-header no-padding">
          <div class="table-header ">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
				<span class="white">&times;</span>
			</button>
			Menu Form
		</div>
          <form action="<?php echo site_url('admin/dashboard/addMenu') ?>" method="post">
             <div class="table-body">
                <div class="widget-main">
	                <div class="col-xs-12 ">
	                   <div class="col-sm-6" >
	                      <label for="form-field-mask-1">
	                      Name :
	                      </label>
	                      <div class="input-group">
	                         <input class="input-large" type="text" id="register3-email" name="menuName">
	                      </div>
	                   </div>
		                <div class="col-sm-6" >
		                      <label for="form-field-mask-1">
		                      Link :
		                      </label>
		                      <div class="input-group">
		                         <input class="input-large" type="text" id="register3-email" name="menuLink">
		                      </div>
		                </div>
	                </div>
	                <br>
	                <div class="col-xs-12">
		               <div class="col-sm-6" >
		                  <label for="form-field-mask-1">
		                  Title :
		                  </label>
		                  <div class="input-group">
		                     <input class="input-large" type="text" id="register3-email" name="menuTitle">
		                  </div>
		               </div>
	               </div>
                   <hr>
                   <br>
                   <div>
                      <label for="form-field-mask-1">
                      Description :
                      </label>
                      <div class="widget-box widget-color-blue">
                         <div class="widget-header widget-header-small">  </div>
                         <div class="widget-body">
                            <div class="widget-main no-padding">
                               <div class="md-editor" id="1500175451069">
                                  <textarea name="menuDis" data-provide="markdown" data-iconlibrary="fa" rows="6" class="md-input" style="resize: none;"></textarea>
                               </div>
                            </div>
                         </div>
                      </div>
                   </div>
                   <br>
                   <div class="modal-footer" style="height: 10%;">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
                    <button class="btn btn-sm btn-primary" type="submit"><i class="ace-icon fa fa-floppy-o bigger-125"></i> Save</button>
                 </div>
                </div>
             </div>
          </form>
       </div>
    </div>
 </div>
</div>
<!-- END Small Modal -->

<table id="simple-table" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th class="center">
				<label class="pos-rel">
					<input type="checkbox" class="ace" />
					<span class="lbl"></span>
				</label>
			</th>
			<th>SL</th>
			<!-- <th class="text-center">Profile</th> -->
			<th>Menu Name</th>
			<th>Menu Link</th>
			<th>Menu Title</th>
			<th>Discription</th>
			<th class="hidden-480">Status</th>
			<th class="text-center">Action</th>
		</tr>
	</thead>

	<tbody>

		<?php $i = 1; $k = 1; $l = 1; foreach ($menu as $v): ?>

			<tr>
				<td class="center">
					<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>
				</td>
				<td><?php echo $k++ ?></td>
				<td style="width: 10%"><?php echo $v->menuName ?></td>
				<td><?php echo $v->menuLink ?></td>
				<td style="width: 10%"><?php echo $v->menuTitle ?></td>
				<td><?php echo $v->menuDis ?></td>
				<td class="hidden-480" style="width: 10%">
				<?php if($v->menuStatus == 1): ?>
					<span class="label label-success arrowed-in arrowed-in-right">Active</span>
					<i style="color: green" class="ace-icon fa fa-check-circle fa-1x icon-animated-bell"></i>

				<?php elseif ($v->menuStatus == 2): ?>

					<span class="label label-sm label-warning">Pending</span>

				<?php else: ?>
            		<i style="color: red" class="ace-icon fa fa-check-circle fa-1x icon-animated-bell"></i>
					<span class="label label-danger arrowed-in">Inactive</span>

	
				<?php endif ?>
				</td>
				<td style="width: 10%;">
					<div class="hidden-sm hidden-xs action-buttons">
					    <a class="btn btn-xs btn-success" id="show-details-btn<?php echo $i++ ?>">
					    	<i class="ace-icon fa fa-pencil-square-o bigger-130"></i>
							
					    </a>

						<a class="btn btn-danger btn-xs red" href="#modal-table<?php echo $v->menuId?>" data-toggle="modal">
						
							<i class="ace-icon fa fa-trash-o bigger-130"></i>
						</a>
						
					</div>
					<div>
					    
						
					</div>
				</td>
			</tr>
			<tr class="detail-row">
				<td colspan="10">
					<div class="table-detail">
						<div class="row">
							<form method="post" action="<?php echo site_url('admin/dashboard/updateMenu') ?>">
								<input type="hidden" name="currentUrl" value="<?php echo current_url() ?>">
								<input type="hidden" name="menuId" value="<?php echo $v->menuId ?>">

					            <div class="col-sm-6">
									<div class="form-group">
										<label class="col-sm-4 control-label no-padding-right" for="menuName">  Menu Name </label>
										<div class="col-sm-8">
											<input type="text" id="menuName" name="menuName" value="<?php echo $v->menuName ?>" class="form-control" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label no-padding-right" for="menuName">  Menu Link </label>
										<div class="col-sm-8">
											<input type="text" id="menuLink" name="menuLink" value="<?php echo $v->menuLink ?>" class="form-control" />
										</div>
									</div>
								</div>

								<div class="col-sm-6">
									<div class="form-group">
										<label class="col-sm-4 control-label no-padding-right" for="menuName">  Menu Title </label>
										<div class="col-sm-8">
											<input type="text" id="menuName" name="menuTitle" value="<?php echo $v->menuTitle ?>" class="form-control" />
										</div>
									</div>
									<!-- <div class="form-group">
										<label class="col-sm-4 control-label no-padding-right" for="menuName">  Menu Name </label>
										<div class="col-sm-8">
											<input type="text" id="menuName" name="menuName" value="<?php echo $v->menuName ?>" class="form-control" />
										</div>
									</div> -->
								</div>

								<div class="clearfix"></div>
								<div class="space-4"></div>

					            <div class="col-sm-12">
									<div class="form-group">
										<div class="col-sm-12">
											<textarea name="menuDis" class="text-editor" id="Description "> <?php echo $v->menuDis ?> </textarea>
										</div>
									</div>
								</div>
								
								<div class="clearfix"></div>
								<div class="space-4"></div>
								<div class="hr hr-dotted"></div>

								<div class="col-sm-12">
									<div class="row">
										<div class="col-sm-8">
											<div class="clearfix pull-right">
												<label>
													<input class="ace" name="status" value="1" <?php if ($v->menuStatus == 1): echo 'checked="checked"'; endif ?> type="radio">
													<span class="lbl"> Active</span>
												</label>
												<label>
													<input class="ace" name="status" value="0" <?php if ($v->menuStatus == 0): echo 'checked="checked"'; endif ?> type="radio">
													<span class="lbl"> Inactive</span>
												</label>
											</div>
										</div>
										<div class="col-sm-4">
											<button class="pull-right btn btn-sm btn-primary btn-white btn-round" type="submit">
												Submit
												<i class="ace-icon fa fa-arrow-right icon-on-right bigger-110"></i>
											</button>
										</div>
									</div>
								</div>
					             
								
						    </form>	
						</div>
					</div>
				</td>
			</tr>
			<div id="modal-table<?php echo $v->menuId?>" class="modal fade" tabindex="-1">
				<div class="modal-dialog" style="width: 400px">
					<div class="modal-content">
						<div class="modal-header no-padding">
							<div class="table-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									<span class="white">&times;</span>
								</button>
								Are you sure?
							</div>
							<div class="modal-footer">
					            <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Cancel</button>
					             <a href="<?php echo site_url('admin/dashboard/deleteAction/'.$v->menuId)?>" class="btn btn-sm btn-primary" type="button"><i class="fa fa-check"></i> Delete</a>
					        </div>
						</div>

						<div class="modal-body no-padding">
							
						</div>
					</div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			</div>
			

		<?php endforeach ?>
    
	</tbody>
</table>

<script src="<?php echo base_url('resource/back-end/js/jquery-2.1.4.min.js') ?>"></script>
<?php $i = 1; foreach ($menu as $v): ?>
	<script type="text/javascript">
		jQuery(function($) {
			$('#show-details-btn<?php echo $i++?>').on('click', function(e) {
				e.preventDefault();
				$(this).closest('tr').next().toggleClass('open');
				/*$(this).find(ace.vars['.icon']).toggleClass('fa-angle-double-down').toggleClass('fa-angle-double-up');*/
			});
		});
	</script>
<?php endforeach ?>
