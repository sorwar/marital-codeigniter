
<table id="simple-table" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th class="center">
				<label class="pos-rel">
					<input type="checkbox" class="ace" />
					<span class="lbl"></span>
				</label>
			</th>
			<th>SL</th>
			<!-- <th class="text-center">Profile</th> -->
			<th>Logo</th>
			<th>Background Image</th>
			<th>Head Title</th>
			<th>Footer Title</th>
			<th class="hidden-480">Status</th>
			<th class="text-center">Action</th>
		</tr>
	</thead>

	<tbody>
			<tr>
				<td class="center">
					<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>
				</td>
				<td><?php echo '1' ?></td>
				<td style="width: 10%"><img style="width: 50px;height: 50px" src="<?php echo base_url('uploads/'.$basic->logo) ?>" ></td>
				<td><img style="width: 50px;height: 50px" src="<?php echo base_url('uploads/'.$basic->background) ?>" ></td>
				<td style="width: 20%"><?php echo $basic->headTitle ?></td>
				<td><?php echo $basic->footerTitle ?></td>
				<td class="hidden-480" style="width: 10%">
				<?php if($basic->status == 1): ?>
					<span class="label label-success arrowed-in arrowed-in-right">Active</span>
					<i style="color: green" class="ace-icon fa fa-check-circle fa-1x icon-animated-bell"></i>

				<?php elseif ($basic->status == 2): ?>

					<span class="label label-sm label-warning">Pending</span>

				<?php else: ?>
            		<i style="color: red" class="ace-icon fa fa-check-circle fa-1x icon-animated-bell"></i>
					<span class="label label-danger arrowed-in">Inactive</span>

	
				<?php endif ?>
				</td>
				<td>
					<div class="hidden-sm hidden-xs action-buttons">
					    <a class="btn btn-xs btn-success" id="show-details-btn">
					    	<i class="ace-icon fa fa-pencil-square-o bigger-130"></i>
					    </a>	
					</div>
				</td>
			</tr>
			<tr class="detail-row">
				<td colspan="10">
					<div class="table-detail">
						<div class="row">
							<form method="post" action="<?php echo site_url('admin/dashboard/updateBasic') ?>" enctype='multipart/form-data' >
								<input type="hidden" name="currentUrl" value="<?php echo current_url() ?>">
								<input type="hidden" name="id" value="<?php echo $basic->id ?>">

					            <div class="col-sm-6">
									<div class="form-group">
										<label class="col-sm-4 control-label no-padding-right" for="headTitle"> Header Title : : </label>
										<div class="col-sm-8">
											<input type="text" id="headTitle" name="headTitle" value="<?php echo $basic->headTitle ?>" class="form-control" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label no-padding-right" for="facebook">  Facebook : </label>
										<div class="col-sm-8">
											<input type="text" id="facebook" name="facebook" value="<?php echo $basic->facebook ?>" class="form-control" />
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label class="col-sm-4 control-label no-padding-right" for="footerTitle"> Footer Title :</label>
										<div class="col-sm-8">
											<input type="text" id="footerTitle" name="footerTitle" value="<?php echo $basic->footerTitle ?>" class="form-control" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label no-padding-right" for="twitter">  Twitter : </label>
										<div class="col-sm-8">
											<input type="text" id="twitter" name="twitter" value="<?php echo $basic->twitter ?>" class="form-control" />
										</div>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="space-4"></div>
								<div class="col-sm-6">
									<div class="form-group">
										<label class="col-sm-4 control-label no-padding-right" for="google"> Google :</label>
										<div class="col-sm-8">
											<input type="text" id="google" name="google" value="<?php echo $basic->google ?>" class="form-control" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label no-padding-right" for="about"> Short About : </label>
										<div class="col-sm-8">
											<textarea style="width: 100%" name="about" class="" id="about "> <?php echo $basic->about ?> </textarea>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label class="col-sm-4 control-label no-padding-right" for="youtube"> Youtube :</label>
										<div class="col-sm-8">
											<input type="text" id="youtube" name="youtube" value="<?php echo $basic->youtube ?>" class="form-control" />
										</div>
									</div>
									<div class="clearfix"></div>
								<div class="space-4"></div>
									<div class="form-group">
										<label class="col-sm-4 control-label no-padding-right" for="map">  Map : </label>
										<div class="col-sm-8">
											<input type="text" id="map" name="map" value="<?php echo $basic->map ?>" class="form-control" />
										</div>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="space-4"></div>

								<div class="col-sm-6">
									<div class="form-group">
										<label class="col-sm-4 control-label no-padding-right" for="logo"> Logo :</label>
										<div class="col-sm-8">
											<input type="file" id="logo" name="logo" class="form-control" />
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="col-sm-4 control-label no-padding-right" for="background"> Background :</label>
										<div class="col-sm-8">
											<input type="file" id="background" name="background" class="form-control" />
										</div>
									</div>
								</div>

								<div class="clearfix"></div>
								<div class="space-4"></div>

					            <div class="col-sm-12">
									<div class="form-group">
										<div class="col-sm-12">
											
										</div>
									</div>
								</div>
								
								<div class="clearfix"></div>
								<div class="space-4"></div>
								<div class="hr hr-dotted"></div>

								<div class="col-sm-12">
									<div class="row">
										<div class="col-sm-12">
											<button class="pull-right btn btn-sm btn-primary btn-white btn-round" type="submit">
												Update
												<i class="ace-icon fa fa-arrow-right icon-on-right bigger-110"></i>
											</button>
										</div>
									</div>
								</div>	
						    </form>	
						</div>
					</div>
				</td>
			</tr> 
	</tbody>
</table>

<script src="<?php echo base_url('resource/back-end/js/jquery-2.1.4.min.js') ?>"></script>
	<script type="text/javascript">
		jQuery(function($) {
			$('#show-details-btn').on('click', function(e) {
				e.preventDefault();
				$(this).closest('tr').next().toggleClass('open');
				/*$(this).find(ace.vars['.icon']).toggleClass('fa-angle-double-down').toggleClass('fa-angle-double-up');*/
			});
		});
	</script>

