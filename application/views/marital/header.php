

<style type="text/css">
   .green2::after {
   color: #fff;
   display: inline-block;
   font-family: "fontawesome";
   font-size: 20px;
   font-variant: normal;
   font-weight: normal;
   line-height: 1;
   margin-left: 0;
   position: relative;
   text-shadow: none;
   text-transform: none;
   top: 11px;
   transition: all 0.4s ease 0s;
   }
   .face {
   display: block;
   margin: auto;
   border-radius: 100%;
   border: 3px solid #fff;
   }
</style>
<div class="navbar navbar-inverse-blue navbar">
   <!--<div class="navbar navbar-inverse-blue navbar-fixed-top">-->
   <div class="navbar-inner">
      <div class="container">
         <div class="navigation">
            <nav id="colorNav">
               <ul>
               <?php 

               $memUname   = $this->session->userData('memUname');
               $session    = $this->M_crud->find('members', array('status' => '1', 'memUname' => $memUname));

               if (empty($memUname)): ?>
                  <li class="green">
                  <a href="javascript::" class="icon-home"></a>
                  <ul>
                     <li><a href="<?php echo site_url('home/login') ?>">Login</a></li>
                     <li><a href="<?php echo site_url('home/register') ?>">Register</a></li>
               <?php else: ?>
                     <li class="green2">
                        <?php 
                           
                               $q = $this->M_crud->find('members', array('status' => '1', 'memUname' => $memUname));

                               $proPic       = $q->memImage1;
                               $memId        = $q->memId;
                           ?>
                        <?php if (!empty($proPic)): ?>
                           <a href="<?php echo site_url('members/viewProfile/'.$memId) ?>" class="icon-home face"><img class="img-circle" height="40" width="40" src="<?php echo base_url('uploads/'.$proPic) ?>"> </a> 
                        <?php else: ?>
                           <a href="<?php echo site_url('members/viewProfile/'.$memId) ?>" class="icon-home face"><img class="img-circle" height="40" width="40" src="<?php echo base_url('resource/front-end/Images/default.jpg') ?>"></a>
                        <?php endif; ?>
                        <ul>                           
                           <li><a href="<?php echo site_url('members/updateInfo/'.$memId) ?>">Update Information</a></li>
                           <li><a href="<?php echo site_url('members/inbox') ?>">Inbox</a></li>
                           <li><a href="<?php echo site_url('members') ?>">Viewer Lists</a></li>
                           <li><a href="<?php echo site_url('home/logout') ?>">Logout</a></li>
               <?php endif; ?>
                        </ul>
                     </li>
                  </ul>
            </nav>
         </div>
         <a class="brand" href="<?php echo site_url('home') ?>"><img style="width: 12%;" src="<?php echo base_url('uploads/'.$basicInfo->logo) ?>"</a>
         <div class="pull-right">
         <nav class="navbar nav_bottom" role="navigation">
         <!-- Brand and toggle get grouped for better mobile display -->
         <div class="navbar-header nav_2">
         <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">Menu
         <span class="sr-only">Toggle navigation</span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         </button>
         <a class="navbar-brand" href="#"></a>
         </div>
         <!-- Collect the nav links, forms, and other content for toggling -->
         <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
         <ul class="nav navbar-nav nav_1">
            <li><a href="<?php echo site_url('home') ?>">Home</a></li>
            <li><a href="<?php echo site_url('members') ?>">Members </a></li>
            <?php

            $menu    = $this->M_crud->findAll('menu', array('menuStatus' => '1'));

            foreach ($menu as $v):
                               ?>
               <li><a href="<?php echo site_url('home/menu/'.$v->menuId) ?>"><?php echo $v->menuName ?></a></li>
            <?php endforeach ?>

            <?php if (!empty($this->session->userData('memUname'))): ?>
               <li><a href="<?php echo site_url('members/inbox') ?>">Messages &nbsp;<i class="fa fa-envelope fa-1x" aria-hidden="true"></i></a></li>
               <a href="viewProfile.php?shadow=<?php echo $memId ?>"></a>
            <?php endif; ?>
            <li><a href="<?php echo site_url('contact') ?>">Contact Us </a></li>   
         </ul>
         </div>
         <!-- /.navbar-collapse -->
         </nav>
         </div>
         <!-- end pull-right -->
         <div class="clearfix"> </div>
      </div>
      <!-- end container -->
   </div>
   <!-- end navbar-inner -->
</div>

