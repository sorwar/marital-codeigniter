<!DOCTYPE HTML>
<html>
  <?php $this->load->view('marital/head') ?>
   <body>
      <!-- ============================  Navigation Start =========================== -->
      <?php $this->load->view('marital/header') ?>
      <!-- ============================  Navigation End ============================ -->
      <div class="grid_3">
         <div class="container">
            <div class="breadcrumb1">
               <ul>
                  <a href="index.php"><i class="fa fa-home home_1"></i></a>
                  <span class="divider">&nbsp;|&nbsp;</span>
                  <li class="current-page"><?php echo $menu->menuTitle ?></li>
               </ul>
            </div>
            <div class="grid_5">
               <p><?php echo $menu->menuDis?></p>
            </div>
         </div>
      </div>
        <?php $this->load->view('marital/footer') ?>
   </body>
</html>

