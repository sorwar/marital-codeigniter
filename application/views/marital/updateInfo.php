

<!DOCTYPE HTML>
<html>
   <?php $this->load->view('marital/head') ?>
   <body>
      <!-- ResponsiveW3layouts -->
      </div>
      <!-- ============================  Navigation Start =========================== -->
      <?php $this->load->view('marital/header') ?>
      <!-- ============================  Navigation End ============================ -->
      <div class="grid_3">
         <div class="container">
            <div class="breadcrumb1">
               <ul>
                  <a href="index.php"><i class="fa fa-home home_1"></i></a>
                  <span class="divider">&nbsp;|&nbsp;</span>
                  <li class="current-page"><a href="<?php echo site_url('members/viewProfile/'.$memInfo->memId) ?>">View Profile</a></li>
               </ul>
            </div>
            <div class="profile">
               <div class="col-md-8 profile_left">
                  <form action="<?php echo site_url('members/adMemberInfo') ?>" method="post" enctype='multipart/form-data'>
                     <input type="hidden" name="memId" value="<?php echo $memInfo->memId ?>" >
                     <h2>Profile Id :&nbsp;<?php echo $memInfo->memProId ?></h2>
                     <div class="col_3">
                        <style type="text/css">
                           .image-preview1 {
                           width: 150px;
                           height: 150px;
                           position: relative;
                           overflow: hidden;
                           <?php if (!empty($memInfo->memImage1)): ?>
                           background-image: url(<?php echo base_url('uploads/'.$memInfo->memImage1) ?>);
                           background-size: 150px 150px;
                           background-repeat: no-repeat;
                           <?php else: ?>
                           background-image: url(<?php echo base_url('resource/front-end/images/up.png') ?>);
                           <?php endif ?>
                           color: #ecf0f1;
                           }
                           .image-preview2 {
                           width: 150px;
                           height: 150px;
                           position: relative;
                           overflow: hidden;
                           <?php if (!empty($memInfo->memImage2)): ?>
                           background-image: url(<?php echo base_url('uploads/'.$memInfo->memImage2) ?>);
                           background-size: 150px 150px;
                           background-repeat: no-repeat;
                           <?php else: ?>
                           background-image: url(<?php echo base_url('resource/front-end/images/up.png') ?>);
                           <?php endif ?>
                           color: #ecf0f1;
                           }
                           .image-preview3 {
                           width: 150px;
                           height: 150px;
                           position: relative;
                           overflow: hidden;
                           <?php if (!empty($memInfo->memImage3)): ?>
                           background-image: url(<?php echo base_url('uploads/'.$memInfo->memImage3) ?>);
                           background-size: 150px 150px;
                           background-repeat: no-repeat;
                           <?php else: ?>
                           background-image: url(<?php echo base_url('resource/front-end/images/up.png') ?>);
                           <?php endif ?>
                           color: #ecf0f1;
                           }
                           .image-preview4 {
                           width: 150px;
                           height: 150px;
                           position: relative;
                           overflow: hidden;
                           <?php if (!empty($memInfo->memImage4)): ?>
                           background-image: url(<?php echo base_url('uploads/'.$memInfo->memImage4) ?>);
                           background-size: 150px 150px;
                           background-repeat: no-repeat;
                           <?php else: ?>
                           background-image: url(<?php echo base_url('resource/front-end/images/up.png') ?>);
                           <?php endif ?>
                           color: #ecf0f1;
                           }
                           .image-preview1 input {
                           line-height: 80px;
                           font-size: 80px;
                           position: absolute;
                           opacity: 0;
                           z-index: 10;
                           }
                           .image-preview2 input {
                           line-height: 80px;
                           font-size: 80px;
                           position: absolute;
                           opacity: 0;
                           z-index: 10;
                           }
                           .image-preview3 input {
                           line-height: 80px;
                           font-size: 80px;
                           position: absolute;
                           opacity: 0;
                           z-index: 10;
                           }
                           .image-preview4 input {
                           line-height: 80px;
                           font-size: 80px;
                           position: absolute;
                           opacity: 0;
                           z-index: 10;
                           }
                           .image-preview1 label {
                           position: absolute;
                           z-index: 5;
                           opacity: 0.8;
                           padding-top: 4px;
                           cursor: pointer;
                           background-color: #bdc3c7;
                           width: 100px;
                           height: 25px;
                           font-size: 12px;
                           line-height: auto;
                           text-transform: uppercase;
                           top: 20px;
                           left: 0;
                           right: 0;
                           bottom: 0;
                           margin: auto;
                           text-align: center;
                           }
                           .image-preview2 label {
                           position: absolute;
                           z-index: 5;
                           opacity: 0.8;
                           padding-top: 4px;
                           cursor: pointer;
                           background-color: #bdc3c7;
                           width: 100px;
                           height: 25px;
                           font-size: 12px;
                           line-height: auto;
                           text-transform: uppercase;
                           top: 20px;
                           left: 0;
                           right: 0;
                           bottom: 0;
                           margin: auto;
                           text-align: center;
                           }
                           .image-preview3 label {
                           position: absolute;
                           z-index: 5;
                           opacity: 0.8;
                           padding-top: 4px;
                           cursor: pointer;
                           background-color: #bdc3c7;
                           width: 100px;
                           height: 25px;
                           font-size: 12px;
                           line-height: auto;
                           text-transform: uppercase;
                           top: 20px;
                           left: 0;
                           right: 0;
                           bottom: 0;
                           margin: auto;
                           text-align: center;
                           }
                           .image-preview4 label {
                           position: absolute;
                           z-index: 5;
                           opacity: 0.8;
                           padding-top: 4px;
                           cursor: pointer;
                           background-color: #bdc3c7;
                           width: 100px;
                           height: 25px;
                           font-size: 12px;
                           line-height: auto;
                           text-transform: uppercase;
                           top: 20px;
                           left: 0;
                           right: 0;
                           bottom: 0;
                           margin: auto;
                           text-align: center;
                           }
                        </style>
                        <div class="col-sm-3 row_2">
                           <div class="image-preview1" id="image-preview1">
                              <label for="image-upload" id="image-label1">Choose File</label>
                              <input type="file" name="memImage1" id="image-upload1" />
                           </div>
                           <div class="image-preview3" id="image-preview3">
                              <label for="image-upload" id="image-label3">Choose File</label>
                              <input type="file" name="memImage3" id="image-upload3" />
                           </div>
                        </div>
                        <div class="col-sm-3 row_2">
                           <div class="image-preview2" id="image-preview2">
                              <label for="image-upload" id="image-label2">Choose File</label>
                              <input type="file" name="memImage2" id="image-upload2" />
                           </div>
                           <div class="image-preview4" id="image-preview4">
                              <label for="image-upload" id="image-label4">Choose File</label>
                              <input type="file" name="memImage4" id="image-upload4" />
                           </div>
                        </div>
                        <div class="col-sm-6 row_3">
                           <table class="table_working_hours">
                              <tbody>
                                 <tr class="opened_1">
                                    <td class="day_label">Username :</td>
                                    <td class="day_value"><?php echo $memInfo->memUname ?></td>
                                 </tr>
                                 <tr class="opened_1">
                                    <td class="day_label">Full Name :</td>
                                    <td class="day_value"><?php echo $memInfo->memName ?></td>
                                 </tr>
                                 <tr class="opened">
                                    <td class="day_label">Email :</td>
                                    <td class="day_value"><?php echo $memInfo->memEmail ?></td>
                                 </tr>
                                 <tr class="opened">
                                    <td class="day_label">Age :</td>
                                    <td class="day_value"><?php echo $memInfo->memAge ?></td>
                                 </tr>
                                 <tr class="opened">
                                    <td class="day_label">Sex :</td>
                                    <td class="day_value"><?php echo $memInfo->memSex ?></td>
                                 </tr>
                                 <tr class="opened">
                                    <td class="day_label">Blood Group :</td>
                                    <td class="day_value"><?php echo $memInfo->memBlood ?></td>
                                 </tr>
                                 <tr class="opened">
                                    <td class="day_label">Religion :</td>
                                    <td class="day_value"><?php echo $memInfo->memReligion ?></td>
                                 </tr>
                                 <tr class="opened">
                                    <td class="day_label">Education :</td>
                                    <td class="day_value"><?php echo $memInfo->memEduDetail ?></td>
                                 </tr>
                                 <tr class="opened">
                                    <td class="day_label">Occupasion :</td>
                                    <td class="day_value"><?php echo $memInfo->memOccDetail ?></td>
                                 </tr>
                                 <tr class="opened">
                                    <td class="day_label">City/location :</td>
                                    <td class="day_value"><?php echo $memInfo->memBirthPlace ?></td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                        <div class="clearfix"> </div>
                     </div>
                     <div class="col_4">
                        <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
                           <input type="hidden" value="<?php echo $memInfo->memId ?>" name="memId">
                           <ul id="myTab" class="nav nav-tabs nav-tabs1" role="tablist">
                              <li role="presentation" class="active"><a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">About Myself</a></li>
                              <li role="presentation"><a href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile">Family Details</a></li>
                              <li role="presentation"><a href="#profile1" role="tab" id="profile-tab1" data-toggle="tab" aria-controls="profile1">Partner Preference</a></li>
                           </ul>
                           <div id="myTabContent" class="tab-content">
                              <div role="tabpanel" class="tab-pane fade in active" id="home" aria-labelledby="home-tab">
                                 <div class="basic_1">
                                    <h3>Basics & Lifestyle</h3>
                                    <div class="col-md-6 basic_1-left">
                                       <table class="table_working_hours">
                                          <tbody>
                                             <tr class="opened_1">
                                                <td class="day_label"> Full Name :</td>
                                                <td class="day_value">
                                                   <input type="text" value="<?php echo $memInfo->memName ?>" name="memName" > 
                                                </td>
                                             </tr>
                                             <tr class="opened" required">
                                                <td class="day_label"> Marital Status :</td>
                                                <td class="select-block1">
                                                   <select style="width: 94%" name="memMarital">
                                                      <option <?php if ($memInfo->memMarital == 'Single'): echo 'selected'; endif ?> value="Single">Single</option>
                                                      <option <?php if ($memInfo->memMarital == 'Divorced'): echo 'selected'; endif ?> value="Divorced">Divorced</option>
                                                      <option <?php if ($memInfo->memMarital == 'Separated'): echo 'selected'; endif ?> value="Separated">Separated</option>
                                                      <option <?php if ($memInfo->memMarital == 'Other'): echo 'selected'; endif ?> value="Other">Other</option>
                                                   </select>
                                                </td>
                                             </tr>
                                             <tr class="opened">
                                                <td class="day_label">Body Type :</td>
                                                <td class="day_value">
                                                   <input type="text" value="<?php echo $memInfo->memBody ?>" name="memBody" > 
                                                </td>
                                             </tr>
                                             <tr class="opened">
                                                <td class="day_label">Height :</td>
                                                <td class="day_value">
                                                   <input type="text" value="<?php echo $memInfo->memHeight ?>" name="memHeight" placeholder="5ft 9 in" > 
                                                </td>
                                             </tr>
                                             <tr class="opened">
                                                <td class="day_label">Physical Status :</td>
                                                <td class="day_value closed"><span><input type="text" value="<?php echo $memInfo->memPhysical ?>" name="memPhysical" > </span></td>
                                             </tr>
                                             <tr class="opened">
                                                <td class="day_label">Profile Created by :</td>
                                                <td class="day_value closed"><span><input type="text" value="<?php echo $memInfo->memCreated ?>" name="memCreated" > </span></td>
                                             </tr>
                                             <tr class="opened">
                                                <td class="day_label">Drink :</td>
                                                <td class="day_value closed"><span><input type="text" value="<?php echo $memInfo->memDrink ?>" name="memDrink" > </span></td>
                                             </tr>
                                          </tbody>
                                       </table>
                                    </div>
                                    <div class="col-md-6 basic_1-left">
                                       <table class="table_working_hours">
                                          <tbody>
                                             <tr class="opened">
                                                <td class="day_label">Age :</td>
                                                <td class="day_value">
                                                   <input type="text" value="<?php echo $memInfo->memAge ?>" name="memAge" >
                                                </td>
                                             </tr>
                                             <tr class="opened">
                                                <td class="day_label">Mother Tongue :</td>
                                                <td class="day_value">
                                                   <input type="text" value="<?php echo $memInfo->memTongue ?>" name="memTongue" > 
                                                </td>
                                             </tr>
                                             <tr class="opened">
                                                <td class="day_label">Complexion :</td>
                                                <td class="day_value">
                                                   <input type="text" value="<?php echo $memInfo->memComplexion ?>" name="memComplexion" > 
                                                </td>
                                             </tr>
                                             <tr class="opened">
                                                <td class="day_label">Weight :</td>
                                                <td class="day_value">
                                                   <input type="text" value="<?php echo $memInfo->memWeight ?>" name="memWeight" > 
                                                </td>
                                             </tr>
                                             <tr class="opened">
                                                <td class="day_label">Blood Group :</td>
                                                <td class="day_value">
                                                   <input type="text" value="<?php echo $memInfo->memBlood ?>" name="memBlood" > 
                                                </td>
                                             </tr>
                                             <tr class="closed">
                                                <td class="day_label">Diet :</td>
                                                <td class="day_value closed"><span><input type="text" value="<?php echo $memInfo->memDiet ?>" name="memDiet" > </span></td>
                                             </tr>
                                             <tr class="closed">
                                                <td class="day_label">Smoke :</td>
                                                <td class="day_value closed"><span><input type="text" value="<?php echo $memInfo->memSmoke ?>" name="memSmoke" > </span></td>
                                             </tr>
                                          </tbody>
                                       </table>
                                    </div>
                                    <div class="clearfix"> </div>
                                 </div>
                                 <div class="basic_1">
                                    <h3>Religious / Social & Astro Background</h3>
                                    <div class="col-md-6 basic_1-left">
                                       <table class="table_working_hours">
                                          <tbody>
                                             <tr class="opened">
                                                <td class="day_label">Time of Birth :</td>
                                                <td class="day_value">
                                                   <input type="text" value="<?php echo $memInfo->memBirthTime ?>" name="memBirthTime" > 
                                                </td>
                                             </tr>
                                             <tr class="opened">
                                                <td class="day_label">Caste :</td>
                                                <td class="day_value">
                                                   <input type="text" value="<?php echo $memInfo->memCaste ?>" name="memCaste" > 
                                                </td>
                                             </tr>
                                             <tr class="opened">
                                                <td class="day_label">Date of Birth :</td>
                                                <td class="day_value closed"><span>
                                                   <input style="max-width: 10%;" type="text" value="<?php echo $memInfo->memAgeDate ?>" name="memAgeDate" >/
                                                   <input style="max-width: 20%;" type="text" value="<?php echo $memInfo->memAgeMonth ?>" name="memAgeMonth" >/
                                                   <input style="max-width: 15%;" type="text" value="<?php echo $memInfo->memAgeYear ?>" name="memAgeYear" >
                                                   </span>
                                                </td>
                                             </tr>
                                             <tr class="opened">
                                                <td class="day_label">City /Location :</td>
                                                <td class="day_value closed"><span><input type="text" value="<?php echo $memInfo->memBirthPlace ?>" name="memBirthPlace" required="" > </span></td>
                                             </tr>
                                          </tbody>
                                       </table>
                                    </div>
                                    <div class="col-md-6 basic_1-left">
                                       <table class="table_working_hours">
                                          <tbody>
                                             <tr class="opened_1">
                                                <td class="day_label">Religion :</td>
                                                <td class="day_value">
                                                   <input type="text" value="<?php echo $memInfo->memReligion ?>" name="memReligion" required=""> 
                                                </td>
                                             </tr>
                                             <tr class="opened">
                                                <td class="day_label">Sub Caste :</td>
                                                <td class="day_value">
                                                   <input type="text" value="<?php echo $memInfo->memSubCaste ?>" name="memSubCaste" > 
                                                </td>
                                             </tr>
                                             <tr class="opened">
                                                <td class="day_label">Raasi :</td>
                                                <td class="day_value">
                                                   <input type="text" value="<?php echo $memInfo->memRaasi ?>" name="memRaasi" > 
                                                </td>
                                             </tr>
                                          </tbody>
                                       </table>
                                    </div>
                                    <div class="clearfix"> </div>
                                 </div>
                                 <div class="basic_1 basic_2">
                                    <h3>Education & Career</h3>
                                    <div class="basic_1-left">
                                       <table class="table_working_hours">
                                          <tbody>
                                             <tr class="opened">
                                                <td class="day_label">Education :</td>
                                                <td class="day_value">
                                                   <input type="text" value="<?php echo $memInfo->memEdu ?>" name="memEdu" > 
                                                </td>
                                             </tr>
                                             <tr class="opened">
                                                <td class="day_label">Education Detail :</td>
                                                <td class="day_value">
                                                   <input type="text" value="<?php echo $memInfo->memEduDetail ?>" name="memEduDetail" > 
                                                </td>
                                             </tr>
                                             <tr class="opened">
                                                <td class="day_label">Occupation Detail :</td>
                                                <td class="day_value closed"><span><input type="text" value="<?php echo $memInfo->memOccDetail ?>" name="memOccDetail" > </span></td>
                                             </tr>
                                             <tr class="opened">
                                                <td class="day_label">Annual Income :</td>
                                                <td class="day_value closed"><span><input type="text" value="<?php echo $memInfo->memAnnualIncome ?>" name="memAnnualIncome" > </span></td>
                                             </tr>
                                          </tbody>
                                       </table>
                                    </div>
                                    <div class="clearfix"> </div>
                                 </div>
                              </div>
                              <div role="tabpanel" class="tab-pane fade" id="profile" aria-labelledby="profile-tab">
                                 <div class="basic_3">
                                    <h4>Family Details</h4>
                                    <div class="basic_1 basic_2">
                                       <h3>Basics</h3>
                                       <div class="col-md-12 basic_1-left">
                                          <table class="table_working_hours">
                                             <tbody>
                                                <tr class="opened">
                                                   <td class="day_label">Father's Occupation :</td>
                                                   <td class="day_value">
                                                      <input type="text" value="<?php echo $memInfo->memFaOcc ?>" name="memFaOcc" > 
                                                   </td>
                                                </tr>
                                                <tr class="opened">
                                                   <td class="day_label">Mother's Occupation :</td>
                                                   <td class="day_value">
                                                      <input type="text" value="<?php echo $memInfo->memMaOcc ?>" name="memMaOcc" > 
                                                   </td>
                                                </tr>
                                                <tr class="opened">
                                                   <td class="day_label">No. of Brothers :</td>
                                                   <td class="day_value closed"><span><input type="text" value="<?php echo $memInfo->memBroNum ?>" name="memBroNum" > </span></td>
                                                </tr>
                                                <tr class="opened">
                                                   <td class="day_label">No. of Sisters :</td>
                                                   <td class="day_value closed"><span><input type="text" value="<?php echo $memInfo->memSisNum ?>" name="memSisNum" > </span></td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div role="tabpanel" class="tab-pane fade" id="profile1" aria-labelledby="profile-tab1">
                                 <div class="basic_1 basic_2">
                                    <div class="basic_1-left">
                                       <table class="table_working_hours">
                                          <tbody>
                                             <tr class="opened">
                                                <td class="day_label">Age :</td>
                                                <td class="day_value">
                                                   <input type="text" value="<?php echo $memInfo->memPartAge ?>" name="memPartAge" > 
                                                </td>
                                             </tr>
                                             <tr class="opened">
                                                <td class="day_label">Marital Status :</td>
                                                <td class="day_value">
                                                   <input type="text" value="<?php echo $memInfo->memPartMarital ?>" name="memPartMarital" > 
                                                </td>
                                             </tr>
                                             <tr class="opened">
                                                <td class="day_label">Body Type :</td>
                                                <td class="day_value closed"><span><input type="text" value="<?php echo $memInfo->memPartBody ?>" name="memPartBody" > </span></td>
                                             </tr>
                                             <tr class="opened">
                                                <td class="day_label">Complexion :</td>
                                                <td class="day_value closed"><span><input type="text" value="<?php echo $memInfo->memPartComplexion ?>" name="memPartComplexion" > </span></td>
                                             </tr>
                                             <tr class="opened">
                                                <td class="day_label">Height 5ft 9 in / 175cm :</td>
                                                <td class="day_value closed"><span><input type="text" value="<?php echo $memInfo->memPartHeight ?>" name="memPartHeight" > </span></td>
                                             </tr>
                                             <tr class="opened">
                                                <td class="day_label">
                                                   <!-- hjnk -->
                                                   Diet :
                                                </td>
                                                <td class="day_value closed"><span><input type="text" value="<?php echo $memInfo->memPartHeight ?>" name="memPartManglik" > </span></td>
                                             </tr>
                                             <tr class="opened">
                                                <td class="day_label">Kujadosham / Manglik :</td>
                                                <td class="day_value closed"><span><input type="text" value="<?php echo $memInfo->memPartManglik ?>" name="memPartManglik" > </span></td>
                                             </tr>
                                             <tr class="opened">
                                                <td class="day_label">Religion :</td>
                                                <td class="day_value closed"><span><input type="text" value="<?php echo $memInfo->memPartReligion ?>" name="memPartReligion" > </span></td>
                                             </tr>
                                             <tr class="opened">
                                                <td class="day_label">Caste :</td>
                                                <td class="day_value closed"><span><input type="text" value="<?php echo $memInfo->memPartCaste ?>" name="memPartCaste" > </span></td>
                                             </tr>
                                             <tr class="opened">
                                                <td class="day_label">Mother Tongue :</td>
                                                <td class="day_value closed"><span><input type="text" value="<?php echo $memInfo->memPartTongue ?>" name="memPartTongue" > </span></td>
                                             </tr>
                                             <tr class="opened">
                                                <td class="day_label">Education :</td>
                                                <td class="day_value closed"><span><input type="text" value="<?php echo $memInfo->memPartEducation ?>" name="memPartEducation" > </span></td>
                                             </tr>
                                             <tr class="opened">
                                                <td class="day_label">Occupation :</td>
                                                <td class="day_value closed"><span><input type="text" value="<?php echo $memInfo->memPartOccupation ?>" name="memPartOccupation" > </span></td>
                                             </tr>
                                             <tr class="opened">
                                                <td class="day_label">Country of Residence :</td>
                                                <td class="day_value closed"><span><input type="text" value="<?php echo $memInfo->memPartCountry ?>" name="memPartCountry" > </span></td>
                                             </tr>
                                             <tr class="opened">
                                                <td class="day_label">State :</td>
                                                <td class="day_value closed"><span><input type="text" value="<?php echo $memInfo->memPartState ?>" name="memPartState" > </span></td>
                                             </tr>
                                             <tr class="opened">
                                                <td class="day_label">Residency Status :</td>
                                                <td class="day_value closed"><span><input type="text" value="<?php echo $memInfo->memPartResidency ?>" name="memPartResidency" > </span></td>
                                             </tr>
                                          </tbody>
                                       </table>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="form-actions">
                        <input type="submit" id="edit-submit" name="op" value="SAVE" class="btn_1 submit">
                     </div>
                  </form>
               </div>
               <div class="col-md-4 profile_right">
                  <div class="members_box1">
                     <div class="newsletter">
                        <form action="member/searchAction.php" method="post">
                           <input type="text" name="search" size="30" required="" ">
                           <input type="submit" value="Go">
                        </form>
                     </div>
                     <div class="clearfix"> </div>
                  </div>
                  <div class="view_profile">
                     <h3>View Similar Profiles</h3>
                     <ul class="profile_item">
                     <?php foreach ($similler as $v):
                         ?>
                           <li>
                              <a href="<?php echo site_url('members/viewProfile/'.$v->memId) ?>">
                                 <li class="profile_item-img">
                                    <?php if (!empty($v->memImage1)): ?>
                                       <img src="<?php echo base_url('uploads/'.$v->memImage1) ?>" class="img-responsive" alt=""/>     
                                    <?php else: ?>
                                       <img src="<?php echo base_url('resource/front-end/Images/default.jpg') ?>" class="img-responsive" alt=""/>
                                    <?php endif; ?>
                                 </li>
                                 <li class="profile_item-desc">
                                    <h4><?php echo $v->memProId ?></h4>
                                    <p><?php echo $v->memAge ?> Yrs, <?php echo $v->memOccDetail ?>,<br><?php echo $v->memReligion ?>,<?php echo $v->memBirthPlace ?></p>
                                    <h5>View Full Profile</h5>
                                 </li>
                                 <div class="clearfix"> </div>
                              </a>
                           </li>
                           <br>
                        <?php endforeach; ?>  
                     </ul>
                  </div>
                  <div class="view_profile view_profile1">
                     <div class="view_profile view_profile1">
                     <h3>Members who viewed this profile also viewed</h3>
                     <?php foreach ($suggest as $v ):?>
                        <ul class="profile_item">
                           <a href="<?php echo site_url('members/viewProfile/'.$v->memId) ?>">
                              <li class="profile_item-img">
                                 <?php if (!empty($v->memImage1)): ?>
                                    <img src="<?php echo base_url('uploads/'.$v->memImage1) ?>" class="img-responsive" alt=""/>    
                                 <?php else: ?>
                                    <img src="<?php echo base_url('resource/front-end/Images/default.jpg') ?>" class="img-responsive" alt=""/>
                                 <?php endif; ?>
                              </li>
                              <li class="profile_item-desc">
                                 <h4><?php echo $v->memProId ?></h4>
                                 <p><?php echo $v->memAge ?> Yrs, <?php echo $v->memOccDetail ?>,<br><?php echo $v->memReligion ?>,<?php echo $v->memBirthPlace ?></p>
                                 <h5>View Full Profile</h5>
                              </li>
                              <div class="clearfix"> </div>
                           </a>
                        </ul>
                     <?php endforeach; ?>   
                  </div>
               </div>
               <div class="clearfix"> </div>
            </div>
         </div>
      </div>
      <?php $this->load->view('marital/footer') ?>
      <script defer src="<?php echo base_url('front-end/js/jquery.flexslider.js') ?>"></script>
      <link rel="stylesheet" href="<?php echo base_url('front-end/css/flexslider.css') ?>" type="text/css" media="screen" />
      <script>
         // Can also be used with $(document).ready()
         $(window).load(function() {
             $('.flexslider').flexslider({
                 animation: "slide",
                 controlNav: "thumbnails"
             });
         });
      </script>
      <script type="text/javascript">
         $(document).ready(function() {
           $.uploadPreview({
             input_field: "#image-upload1",   // Default: .image-upload
             preview_box: "#image-preview1",  // Default: .image-preview
             label_field: "#image-label1",    // Default: .image-label
             label_default: "Choose File",   // Default: Choose File
             label_selected: "Change File",  // Default: Change File
             no_label: false                 // Default: false
           });
         });
         $(document).ready(function() {
           $.uploadPreview({
             input_field: "#image-upload2",   // Default: .image-upload
             preview_box: "#image-preview2",  // Default: .image-preview
             label_field: "#image-label2",    // Default: .image-label
             label_default: "Choose File",   // Default: Choose File
             label_selected: "Change File",  // Default: Change File
             no_label: false                 // Default: false
           });
         });
         $(document).ready(function() {
           $.uploadPreview({
             input_field: "#image-upload3",   // Default: .image-upload
             preview_box: "#image-preview3",  // Default: .image-preview
             label_field: "#image-label3",    // Default: .image-label
             label_default: "Choose File",   // Default: Choose File
             label_selected: "Change File",  // Default: Change File
             no_label: false                 // Default: false
           });
         });
         $(document).ready(function() {
           $.uploadPreview({
             input_field: "#image-upload4",   // Default: .image-upload
             preview_box: "#image-preview4",  // Default: .image-preview
             label_field: "#image-label4",    // Default: .image-label
             label_default: "Choose File",   // Default: Choose File
             label_selected: "Change File",  // Default: Change File
             no_label: false                 // Default: false
           });
         });
      </script>
   </body>
</html>

