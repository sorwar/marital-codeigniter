

<!DOCTYPE HTML>
<html>
      <?php $this->load->view('marital/head') ?>
      <body>
            <!-- ResponsiveW3layouts -->
            <!-- ============================  Navigation Start =========================== -->
            <?php $this->load->view('marital/header')  ?> 
            <!-- ============================  Navigation End ============================ -->
                  <div class="col-md-3 members_box1">
                        <div class="view_profile">
                           <h3>New Members</h3>
                           <ul class="profile_item">
                           <?php foreach ($similler as $v):
                               ?>
                                 <li>
                                    <a href="<?php echo site_url('members/viewProfile/'.$v->memId) ?>">
                                       <li class="profile_item-img">
                                          <?php if (!empty($v->memImage1)): ?>
                                             <img src="<?php echo base_url('uploads/'.$v->memImage1) ?>" class="img-responsive" alt=""/>     
                                          <?php else: ?>
                                             <img src="<?php echo base_url('resource/front-end/Images/default.jpg') ?>" class="img-responsive" alt=""/>
                                          <?php endif; ?>
                                       </li>
                                       <li class="profile_item-desc">
                                          <h4><?php echo $v->memProId ?></h4>
                                          <p><?php echo $v->memAge ?> Yrs, <?php echo $v->memOccDetail ?>,<br><?php echo $v->memReligion ?>,<?php echo $v->memBirthPlace ?></p>
                                          <h5>View Full Profile</h5>
                                       </li>
                                       <div class="clearfix"> </div>
                                    </a>
                                 </li>
                                 <br>
                              <?php endforeach; ?>  
                           </ul>
                        </div>
                        <div class="view_profile view_profile1">
                           <h3>Other Members</h3>
                           <ul class="profile_item">
                           <?php foreach ($suggest as $v):
                               ?>
                                 <li>
                                    <a href="<?php echo site_url('members/inbox/'.$v->memId) ?>">
                                       <li class="profile_item-img">
                                          <?php if (!empty($v->memImage1)): ?>
                                             <img src="<?php echo base_url('uploads/'.$v->memImage1) ?>" class="img-responsive" alt=""/>     
                                          <?php else: ?>
                                             <img src="<?php echo base_url('resource/front-end/Images/default.jpg') ?>" class="img-responsive" alt=""/>
                                          <?php endif; ?>
                                       </li>
                                       <li class="profile_item-desc">
                                          <h4><?php echo $v->memProId ?></h4>
                                          <p><?php echo $v->memAge ?> Yrs, <?php echo $v->memOccDetail ?>,<br><?php echo $v->memReligion ?>,<?php echo $v->memBirthPlace ?></p>
                                          <h5>View Full Profile</h5>
                                       </li>
                                       <div class="clearfix"> </div>
                                    </a>
                                 </li>
                                 <br>
                              <?php endforeach; ?>  
                           </ul>
                          
                        </div>
                  </div>
                  <!-- col-md-3 col_5 -->
                  <div class="col-md-9 members_box2">
                     <h3>Inbox</h3>
                     <!-728x90--->
                     <div class="col_4">
                        <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
                           <ul id="myTab" class="nav nav-tabs nav-tabs1" role="tablist">
                              <li role="presentation" class="active"><a href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile">New</a></li>
                              <li role="presentation"><a href="#profile1" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile">SENT</a></li>
                           </ul>
                           <div id="myTabContent" class="tab-content">
                              <!-- ..................................#profile..............................			     -->
                              <div role="tabpanel" class="tab-pane fade in active" id="profile" aria-labelledby="home-tab">
                                 <ul class="pagination pagination_1 nav">
                                    <li><a href="#">1</a></li>
                                    <!-- class="active"> -->                 
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">5</a></li>
                                    <li><a href="#">...</a></li>
                                    <li><a href="#">Next</a></li>
                                 </ul>
                                 <div class="clearfix"> </div>
                                 <?php foreach ($inbox as $v): ?>

                                    <?php if ($v->msgAdmin == 'Yes' && $v->msgSender == 0):?>

                                    <div class="jobs-item with-thumb">
                                       <div class="thumb_top">
                                          <div class="jobs_right">
                                             <ul>
                                                <li>
                                                <br>
                                                   <a href="javascript::">
                                                      <h4><img class="img-circle" height="40" width="40" src="<?php echo site_url('resource/front-end/images/default.jpg') ?>">&nbsp;Administrator</h4>
                                                      <h4 style="position: relative;left: 7%;color: black;" class="description"><?php echo $v->msgBox ?></h4>
                                                   </a>
                                                   <br>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="clearfix"> </div>
                                       </div>
                                    </div>

                                 <?php else: ?>

                                    <div class="jobs-item with-thumb">
                                       <div class="thumb_top">
                                          <div class="jobs_right">
                                             <ul>
                                                <li>
                                                <br>
                                                   <a href="<?php echo site_url('members/viewProfile/'.$v->msgSender) ?>">
                                                      <h4><img class="img-circle" height="40" width="40" src="<?php echo site_url('uploads/'.$v->memImage1) ?>">&nbsp;<?php echo $v->memUname ?></h4>
                                                      <h4 style="position: relative;left: 7%;color: black;" class="description"><?php echo $v->msgBox ?></h4>
                                                   </a>
                                                   <br>
                                                </li>
                                             </ul>
                                          </div>
                                          <div class="clearfix"> </div>
                                       </div>
                                    </div>

                                 <?php endif ?>

                                 <?php endforeach ?>
        
                              </div>
                              <!-- ...............................#profile end...................................... -->
                              <!-- ...........................#profile1................................... -->
                              <div role="tabpanel" class="tab-pane fade" id="profile1" aria-labelledby="profile-tab">
                                 <ul class="pagination pagination_1 nav" >
                                    <li ><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">5</a></li>
                                    <li><a href="#">...</a></li>
                                    <li><a href="#">Next</a></li>
                                 </ul> 
                                 <div class="clearfix"> </div>
                                 <?php foreach ($sent as $v): ?>
                                    <div class="jobs-item with-thumb">
                                       <div class="thumb_top">
                                          <div class="jobs_right">
                                             <ul>
                                                <li>
                                                   <a href="<?php echo site_url('members/viewProfile/'.$v->msgReceiver) ?>">
                                                      <h4><img class="img-circle" height="60" width="60" src="<?php echo site_url('uploads/'.$v->memImage1) ?>">&nbsp;<?php echo $v->memUname ?></h4>
                                                      <h4 style="position: relative;left: 7%;color: black;" class="description"><?php echo $v->msgBox ?></h4>
                                                   </a>
                                                </li>
                                             </ul>
                                          </div>
                                       </div>
                                    </div>
                                 <?php endforeach ?>
                                  <div class="clearfix"> </div>
                               
                              </div>  <!-- profile1 -->
                              <!-- ..............................#profile1 end.................................-->
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="clearfix"> </div>
            <!-- footer start -->
            <?php $this->load->view('marital/footer') ?>
            <!-- footer end --> -->
      </body>
</html>

