<head>
   <title><?php echo $basicInfo->headTitle ?></title>
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <meta name="keywords" content="" />
   <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
   <link href="<?php echo base_url('resource/front-end/css/bootstrap-3.1.1.min.css') ?>" rel='stylesheet' type='text/css' />
   <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <script src="<?php echo base_url('resource/front-end/js/jquery.min.js') ?>"></script>
   <script src="<?php echo base_url('resource/front-end/js/bootstrap.min.js') ?>"></script>
   <!-- file upload -->
   <!-- <script type="text/javascript" src="http://code.jquery.com/jquery-2.0.3.min.js') ?>"></script> -->
   <script type="text/javascript" src="<?php echo base_url('resource/front-end/js/jquery.uploadPreview.min.js') ?>"></script>
   <script type="text/javascript" src="<?php echo base_url('resource/front-end/css/file-upload.css') ?>"></script>
   <!-- Custom Theme files -->
   <link href="<?php echo base_url('resource/front-end/css/style.css') ?>" rel='stylesheet' type='text/css' />
   <link href='http://fonts.googleapis.com/css?family=Oswald:300,400,700' rel='stylesheet' type='text/css'>
   <link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700' rel='stylesheet' type='text/css'>
   <link href="<?php echo base_url('resource/front-end/css/font-awesome.css') ?>" rel="stylesheet">
   <script>
      $(document).ready(function(){
          $(".dropdown").hover(            
              function() {
                  $('.dropdown-menu', this).stop( true, true ).slideDown("fast");
                  $(this).toggleClass('open');        
              },
              function() {
                  $('.dropdown-menu', this).stop( true, true ).slideUp("fast");
                  $(this).toggleClass('open');       
              }
          );
      });
   </script>
</head>

