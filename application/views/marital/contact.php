<!DOCTYPE HTML>
<html>
   <?php $this->load->view('marital/head') ?>
   <body>
      <!-- ============================  Navigation Start =========================== -->
      <?php $this->load->view('marital/header') ?>
      <!-- end navbar-inverse-blue -->
      <!-- ============================  Navigation End ============================ -->

      <div class="grid_3">
         <div class="container">
            <div class="breadcrumb1">
               <ul>
                  <a href="index.html"><i class="fa fa-home home_1"></i></a>
                  <span class="divider">&nbsp;|&nbsp;</span>
                  <li class="current-page">Contact</li>
               </ul>
            </div>
            <div class="grid_5">
               <p><?php echo $contactInfo->menuDis ?></p>

               <address class="addr row">
                  <dl class="grid_4">
                     <dt>Address :</dt>
                     <dd>
                        <?php echo $contactInfo->menuName ?>
                     </dd>
                  </dl>
                  <dl class="grid_4">
                     <dt>Telephones :</dt>
                     <dd style="width: 120px;">
                        <?php echo $contactInfo->menuLink ?>
                     </dd>
                  </dl>
                  <dl class="grid_4">
                     <dt>E-mail :</dt>
                     <dd><a href="#"><?php echo $contactInfo->menuTitle ?></a></dd>
                  </dl>
               </address>
            </div>
         </div>
      </div>
      <div class="about_middle">
         <div class="container">
            <h2>Contact Form</h2>
            <form id="contact-form" class="contact-form" method="post" action="<?php echo site_url('contact/contactMail') ?>">
               <fieldset>
                  <input type="text" class="text" placeholder="" name="name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Name';}">
                  <input type="text" class="text" placeholder="" name="phone" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Phone';}">
                  <input type="text" class="text" placeholder="" name="email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}">
                  <textarea name="text" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Message';}">Message</textarea>
                  <input name="submit" type="submit" id="submit" value="Submit">
               </fieldset>
            </form>
         </div>
      </div>
       <?php $this->load->view('marital/footer') ?>
   </body>
</html>

