<!DOCTYPE HTML>
<html>
  <?php $this->load->view('marital/head') ?>
    <body>
      <!-- ============================  Navigation Start =========================== -->
      <?php $this->load->view('marital/header') ?>
      <!-- ============================  Navigation End ============================ -->
        <div class="col-md-3 members_box1">
            <div class="view_profile">
               <h3>New Members</h3>
               <ul class="profile_item">
                  <?php foreach ($similler as $v):?>
                     <li>
                        <a href="<?php echo site_url('members/viewProfile/'.$v->memId) ?>">
                           <li class="profile_item-img">
                              <?php if (!empty($v->memImage1)): ?>
                                 <img src="<?php echo base_url('uploads/'.$v->memImage1) ?>" class="img-responsive" alt=""/>     
                              <?php else: ?>
                                 <img src="<?php echo base_url('resource/front-end/Images/default.jpg') ?>" class="img-responsive" alt=""/>
                              <?php endif; ?>
                           </li>
                           <li class="profile_item-desc">
                              <h4><?php echo $v->memProId ?></h4>
                              <p><?php echo $v->memAge ?> Yrs, <?php echo $v->memOccDetail ?>,<br><?php echo $v->memReligion ?>,<?php echo $v->memBirthPlace ?></p>
                              <h5>View Full Profile</h5>
                           </li>
                           <div class="clearfix"> </div>
                        </a>
                     </li>
                     <br>
                  <?php endforeach; ?>  
               </ul>
            </div>
            <div class="view_profile view_profile1">
               <h3>Other Members</h3>
               <ul class="profile_item">
               <?php foreach ($suggest as $v):
                   ?>
                     <li>
                        <a href="<?php echo site_url('members/viewProfile/'.$v->memId) ?>">
                           <li class="profile_item-img">
                              <?php if (!empty($v->memImage1)): ?>
                                 <img src="<?php echo base_url('uploads/'.$v->memImage1) ?>" class="img-responsive" alt=""/>     
                              <?php else: ?>
                                 <img src="<?php echo base_url('resource/front-end/Images/default.jpg') ?>" class="img-responsive" alt=""/>
                              <?php endif; ?>
                           </li>
                           <li class="profile_item-desc">
                              <h4><?php echo $v->memProId ?></h4>
                              <p><?php echo $v->memAge ?> Yrs, <?php echo $v->memOccDetail ?>,<br><?php echo $v->memReligion ?>,<?php echo $v->memBirthPlace ?></p>
                              <h5>View Full Profile</h5>
                           </li>
                           <div class="clearfix"> </div>
                        </a>
                     </li>
                     <br>
                  <?php endforeach; ?>  
               </ul>
            </div>
        </div>
            <!-- col-md-3 col_5 -->
        <div class="col-md-9 members_box">
             <div class="members_box1">
                <div class="newsletter">
                   <form action="<?php echo site_url('members/search') ?>" method="post">
                      <input type="text" name="search" size="30" required="" ">
                      <input type="submit" value="Go">
                   </form>
                </div>
                <div class="clearfix"> </div>
             </div>
      
             <?php foreach ($members as $v):
                   ?>
             <div class="profile_top">
               <h2><?php echo $v->memProId ?></h2>
               <div class="col-sm-3 profile_left-top">
                  <?php if (!empty($v->memImage1)): ?>
                    <img src="<?php echo base_url('uploads/'.$v->memImage1) ?>" class="img-responsive" alt=""/>     
                  <?php else: ?>
                    <img src="<?php echo base_url('resource/front-end/Images/default.jpg') ?>" class="img-responsive" alt=""/>
                  <?php endif; ?>
               </div>
               <div class="col-sm-3">
                  <ul class="login_details1">
                     <li>
                        <p><?php echo $v->memComm ?></p>
                     </li>
                  </ul>
               </div>
               <div class="col-sm-6">
                  <table class="table_working_hours">
                     <tbody>
                        <tr class="opened_1">
                           <td class="day_label1">Name:</td>
                           <td class="day_value"><?php echo $v->memName ?></td>
                        </tr>
                        <tr class="opened">
                           <td class="day_label1">Sex :</td>
                           <td class="day_value"><?php echo $v->memSex ?></td>
                        </tr>
                        <tr class="opened">
                           <td class="day_label1">Age :</td>
                           <td class="day_value"><?php echo $v->memAge ?></td>
                        </tr>
                        <tr class="opened">
                           <td class="day_label1">Religion :</td>
                           <td class="day_value"><?php echo $v->memReligion?></td>
                        </tr>
                        <tr class="opened">
                           <td class="day_label1">Location :</td>
                           <td class="day_value"><?php echo $v->memBirthPlace ?></td>
                        </tr>
                        <tr class="closed">
                           <td class="day_label1">Education :</td>
                           <td class="day_value closed"><span><?php echo $v->memEdu?></span></td>
                        </tr>
                        <tr class="closed">
                           <td class="day_label1">Occupation :</td>
                           <td class="day_value closed"><span><?php echo $v->memOccDetail?></span></td>
                        </tr>
                     </tbody>
                  </table>
                  <!-- table_working_hours -->
                  <div class="buttons">
                     <div class="vertical">Send Mail</div>
                     <div class="vertical">Shortlisted</div>
                     <div class="vertical">Send Interest</div>
                  </div>
               </div>
               <!-- "col-sm-6" -->
               <div class="clearfix"> </div>
             </div>
             <!-- class="profile_top" -->
             <?php endforeach ?>  
        </div>
        <!-- md-9 members_box -->
      <?php $this->load->view('marital/footer') ?>
    </body>
</html>

