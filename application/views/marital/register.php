

<!DOCTYPE HTML>
<html>
   <?php $this->load->view('marital/head') ?>
   <body>
      <!-- ============================  Navigation Start =========================== -->
      <?php $this->load->view('marital/header') ?>
      <!-- ============================  Navigation End ============================ -->
      <div class="grid_3">
         <div class="container">
            <div class="breadcrumb1">
               <ul>
                  <a href="index.php"><i class="fa fa-home home_1"></i></a>
                  <span class="divider">&nbsp;|&nbsp;</span>
                  <li class="current-page">Register</li>
               </ul>
            </div>
            <?php
               $registered = $this->uri->segment(3);
                  if ($registered == 'registered'):        
               ?>

               <div class="alert alert-success">
                  <strong>sUCCESS!</strong> Registered Successfully. Please wait for &nbsp;<strong>APPROVAL</strong> 
               </div>
               
            <?php elseif ($registered == 'did_match'): ?>

               <div class="alert alert-danger">
                  <strong>Danger!</strong> Username Allrady Exist.
               </div>

            <?php endif ?>

            <div class="services">
               <div class="col-sm-6 login_left">
                  <form action="<?php echo site_url('home/registerAction') ?>" method="post">
                     <div class="form-group">
                        <label for="edit-name">Username <span class="form-required" title="This field is required.">*</span></label>
                        <input type="text" id="edit-name" name="memUname"  size="60" maxlength="60" class="form-text required">
                     </div>
                     <div class="form-group">
                        <label for="edit-pass">Password <span class="form-required" title="This field is required.">*</span></label>
                        <input type="password" id="edit-pass" name="memPass" size="60" maxlength="128" class="form-text required">
                     </div>
                     <div class="form-group">
                        <label for="edit-name">Email <span class="form-required" title="This field is required.">*</span></label>
                        <input type="email" id="edit-name" name="memEmail" size="60" maxlength="60" class="form-text required">
                     </div>
                     <input type="hidden" name="status" value="1" class="form-text required">
                     <div class="age_select" required">
                        <label for="edit-pass">Birthday <span class="form-required" title="This field is required.">*</span></label>
                        <div class="age_grid">
                           <div class="col-sm-4 form_box">
                              <div class="select-block1">
                                 <select name="memAgeDate">
                                    <option value="">Date</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                    <option value="31">31</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-sm-4 form_box2" required">
                              <div class="select-block1">
                                 <select name="memAgeMonth">
                                    <option value="">Month</option>
                                    <option value="January">January</option>
                                    <option value="February">February</option>
                                    <option value="March">March</option>
                                    <option value="April">April</option>
                                    <option value="May">May</option>
                                    <option value="June">June</option>
                                    <option value="July">July</option>
                                    <option value="August">August</option>
                                    <option value="September">September</option>
                                    <option value="October">October</option>
                                    <option value="November">November</option>
                                    <option value="December">December</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-sm-4 form_box1">
                              <div class="select-block1">
                                 <select name="memAgeYear">
                                    <option value="">Year</option>
                                    <option value="1980">1980</option>
                                    <option value="1981">1981</option>
                                    <option value="1982">1982</option>
                                    <option value="1983">1983</option>
                                    <option value="1984">1984</option>
                                    <option value="1985">1985</option>
                                    <option value="1986">1986</option>
                                    <option value="1987">1987</option>
                                    <option value="1988">1988</option>
                                    <option value="1989">1989</option>
                                    <option value="1990">1990</option>
                                    <option value="1991">1991</option>
                                    <option value="1992">1992</option>
                                    <option value="1993">1993</option>
                                    <option value="1994">1994</option>
                                    <option value="1995">1995</option>
                                    <option value="1996">1996</option>
                                    <option value="1997">1997</option>
                                    <option value="1998">1998</option>
                                    <option value="1999">1999</option>
                                    <option value="2000">2000</option>
                                    <option value="2001">2001</option>
                                    <option value="2002">2002</option>
                                    <option value="2003">2003</option>
                                    <option value="2004">2004</option>
                                    <option value="2005">2005</option>
                                    <option value="2006">2006</option>
                                    <option value="2007">2007</option>
                                    <option value="2008">2008</option>
                                    <option value="2009">2009</option>
                                    <option value="2010">2010</option>
                                    <option value="2011">2011</option>
                                    <option value="2012">2012</option>
                                    <option value="2013">2013</option>
                                    <option value="2014">2014</option>
                                    <option value="2015">2015</option>
                                    <option value="2014">2016</option>
                                    <option value="2015">2017</option>
                                 </select>
                              </div>
                           </div>
                           <div class="clearfix"> </div>
                        </div>
                     </div>
                     <div class="age_select" required">
                        <label for="edit-pass">Sex <span class="form-required" title="This field is required.">*</span></label>
                        <div class="age_grid">
                           <div class="col-sm-3 form_box">
                              <div class="select-block1">
                                 <select name="memSex">
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                 </select>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="clearfix"> </div>
                     <br>
                     <div class="age_select" required">
                        <label for="edit-pass">Marital Status <span class="form-required" title="This field is required.">*</span></label>
                        <div class="age_grid">
                           <div class="col-sm-4 form_box">
                              <div class="select-block1">
                                 <select name="memMarital">
                                    <option value="Single">Single</option>
                                    <option value="Divorced">Divorced</option>
                                    <option value="Separated">Separated</option>
                                    <option value="Other">Other</option>
                                 </select>
                              </div>
                           </div>
                        </div>
                     </div>
                     <br>
                     <div class="form-actions">
                        <input type="submit" id="edit-submit"  value="Submit" class="btn_1 submit" >
                     </div>
                  </form>
               </div>
               <div class="col-sm-6">
                  <ul class="sharing">
                     <li><a href="#" class="facebook" title="Facebook"><i class="fa fa-boxed fa-fw fa-facebook"></i> Share on Facebook</a></li>
                     <li><a href="#" class="twitter" title="Twitter"><i class="fa fa-boxed fa-fw fa-twitter"></i> Tweet</a></li>
                     <li><a href="#" class="google" title="Google"><i class="fa fa-boxed fa-fw fa-google-plus"></i> Share on Google+</a></li>
                     <li><a href="#" class="linkedin" title="Linkedin"><i class="fa fa-boxed fa-fw fa-linkedin"></i> Share on LinkedIn</a></li>
                     <li><a href="#" class="mail" title="Email"><i class="fa fa-boxed fa-fw fa-envelope-o"></i> E-mail</a></li>
                  </ul>
               </div>
               <div class="clearfix"> </div>
            </div>
         </div>
      </div>
      <div>
      <?php $this->load->view('marital/footer') ?>
   </body>
</html>

