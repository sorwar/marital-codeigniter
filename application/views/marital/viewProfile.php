

<!DOCTYPE HTML>
<html>
   <?php $this->load->view('marital/head') ?>
   <body>
      <!-- ResponsiveW3layouts -->
      </div>
      <!-- ============================  Navigation Start =========================== -->
      <?php $this->load->view('marital/header') ?>
      <!-- ============================  Navigation End ============================ -->
      <div class="grid_3">
         <div class="container">
            <div class="breadcrumb1">
               <ul>
                  <a href="index.php"><i class="fa fa-home home_1"></i></a>
                  <span class="divider">&nbsp;|&nbsp;</span>
                  <li class="current-page">View Profile</li>
               </ul>
            </div>
            <div class="profile">
               <div class="col-md-8 profile_left">
                  <h2>Profile Id :&nbsp;<?php echo $memInfo->memProId ?></h2>
                  <div class="col_3">
                     <div class="col-sm-4 row_2">
                        <div class="flexslider">
                           <ul class="slides">
                              <?php if (!empty($memInfo->memImage1)): ?>
                              <li data-thumb="<?php echo base_url('uploads/'.$memInfo->memImage1) ?>">
                                 <img src="<?php echo base_url('uploads/'.$memInfo->memImage1) ?>" />
                              </li>
                              <?php else: ?>
                              <li 
                                 data-thumb="<?php echo base_url('resource/front-end/images/default.jpg') ?>">
                                 <img src="<?php echo base_url('resource/front-end/images/default.jpg') ?>" />
                              </li>
                              <?php endif; ?>
                              <?php if (!empty($memInfo->memImage2)): ?>
                              <li
                                 data-thumb="<?php echo base_url('uploads/'.$memInfo->memImage2) ?>">
                                 <img src="<?php echo base_url('uploads/'.$memInfo->memImage2) ?>" />
                              </li>
                              <?php else: ?>
                              <li 
                                 data-thumb="<?php echo base_url('resource/front-end/Images/default.jpg') ?>">
                                 <img src="<?php echo base_url('resource/front-end/Images/default.jpg') ?>" />
                              </li>
                              <?php endif; ?>
                              <?php if (!empty($memInfo->memImage3)): ?>
                              <li 
                                 data-thumb="<?php echo base_url('uploads/'.$memInfo->memImage3) ?>">
                                 <img src="<?php echo base_url('uploads/'.$memInfo->memImage3) ?>" />
                              </li>
                              <?php else: ?>
                              <li 
                                 data-thumb="<?php echo base_url('resource/front-end/Images/default.jpg') ?>">
                                 <img src="<?php echo base_url('resource/front-end/Images/default.jpg') ?>" />
                              </li>
                              <?php endif; ?>
                              <?php if (!empty($memInfo->memImage4)): ?>
                              <li 
                                 data-thumb="<?php echo base_url('uploads/'.$memInfo->memImage4) ?>">
                                 <img src="<?php echo base_url('uploads/'.$memInfo->memImage4) ?>" />
                              </li>
                              <?php else: ?>
                              <li 
                                 data-thumb="<?php echo base_url('resource/front-end/Images/default.jpg') ?>">
                                 <img src="<?php echo base_url('resource/front-end/Images/default.jpg') ?>" />
                              </li>
                              <?php endif; ?>
                           </ul>
                        </div>
                     </div>
                     <div class="col-sm-8 row_1">
                        <table class="table_working_hours">
                           <tbody>
                              <tr class="opened_1">
                                 <td class="day_label">Username :</td>
                                 <td class="day_value"><?php echo $memInfo->memUname ?></td>
                              </tr>
                              <tr class="opened_1">
                                 <td class="day_label">Full Name :</td>
                                 <td class="day_value"><?php echo $memInfo->memName ?></td>
                              </tr>
                              <tr class="opened">
                                 <td class="day_label">Email :</td>
                                 <td class="day_value"><?php echo $memInfo->memEmail ?></td>
                              </tr>
                              <tr class="opened">
                                 <td class="day_label">Age :</td>
                                 <td class="day_value"><?php echo $memInfo->memAge ?></td>
                              </tr>
                              <tr class="opened">
                                 <td class="day_label">Sex :</td>
                                 <td class="day_value"><?php echo $memInfo->memSex ?></td>
                              </tr>
                              <tr class="opened">
                                 <td class="day_label">Blood Group :</td>
                                 <td class="day_value"><?php echo $memInfo->memBlood ?></td>
                              </tr>
                              <tr class="opened">
                                 <td class="day_label">Religion :</td>
                                 <td class="day_value"><?php echo $memInfo->memReligion ?></td>
                              </tr>
                              <tr class="opened">
                                 <td class="day_label">Education :</td>
                                 <td class="day_value"><?php echo $memInfo->memEduDetail ?></td>
                              </tr>
                              <tr class="opened">
                                 <td class="day_label">Occupasion :</td>
                                 <td class="day_value"><?php echo $memInfo->memOccDetail ?></td>
                              </tr>
                              <tr class="opened">
                                 <td class="day_label">City/Location :</td>
                                 <td class="day_value"><?php echo $memInfo->memBirthPlace ?></td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                     <div class="clearfix"> </div>
                  </div>
                  <div class="col_4">
                     <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
                        <input type="hidden" value="<?php echo $memInfo->memId ?>" name="memId">
                        <ul id="myTab" class="nav nav-tabs nav-tabs1" role="tablist">
                           <li role="presentation" class="active"><a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">About Myself</a></li>
                           <li role="presentation"><a href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile">Family Details</a></li>
                           <li role="presentation"><a href="#profile1" role="tab" id="profile-tab1" data-toggle="tab" aria-controls="profile1">Partner Preference</a></li>
                           <?php if (empty($this->session->userData('memUname'))): ?>
                              <li role="presentation">
                                 <a href="<?php echo site_url('home/login/'.'login-first') ?>" id="profile-tab1">
                                 Send Message <i class="fa fa-envelope fa-1x" aria-hidden="true"></i>
                                 </a>
                              </li>
                           <?php else: ?>
                              <?php
                                 $memberId = $this->uri->segment(3);
                                 $msgId = $this->M_crud->find('members', array('memId' => $memberId));

                                 $memUname = $this->session->userData('memUname');
                                 $msgUname = $this->M_crud->find('members', array('memUname' => $memUname));

                               if($msgId->memId == $msgUname->memId):?>
                                 <li role="presentation">
                                    <a href="<?php echo site_url('members/inbox') ?>" id="profile-tab1">
                                    Messages <i class="fa fa-envelope fa-1x" aria-hidden="true"></i>
                                    </a>
                                 </li>
                              <?php else:?>
                                 <li role="presentation">
                                    <a href="#profile4" role="tab" id="profile-tab1" data-toggle="tab" aria-controls="profile1">
                                    Send Message <i class="fa fa-envelope fa-1x" aria-hidden="true"></i>
                                    </a>
                                 </li>
                              <?php endif;?>
                           <?php endif;?>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                           <div role="tabpanel" class="tab-pane fade in active" id="home" aria-labelledby="home-tab">
                              <div class="basic_1">
                                 <h3>Basics & Lifestyle</h3>
                                 <div class="col-md-6 basic_1-left">
                                    <table class="table_working_hours">
                                       <tbody>
                                          <tr class="opened_1">
                                             <td class="day_label">Full Name :</td>
                                             <td class="day_value"> <?php echo $memInfo->memName ?></td>
                                          </tr>
                                          <tr class="opened">
                                             <td class="day_label">Marital Status :</td>
                                             <td class="day_value"><?php echo $memInfo->memMarital ?></td>
                                          </tr>
                                          <tr class="opened">
                                             <td class="day_label">Body Type :</td>
                                             <td class="day_value"><?php echo $memInfo->memBody ?></td>
                                          </tr>
                                          <tr class="opened">
                                             <td class="day_label">Height :</td>
                                             <td class="day_value"><?php echo $memInfo->memHeight ?></td>
                                          </tr>
                                          <tr class="opened">
                                             <td class="day_label">Physical Status :</td>
                                             <td class="day_value closed"><span><?php echo $memInfo->memPhysical ?></span></td>
                                          </tr>
                                          <tr class="opened">
                                             <td class="day_label">Profile Created by :</td>
                                             <td class="day_value closed"><span><?php echo $memInfo->memCreated ?></span></td>
                                          </tr>
                                          <tr class="opened">
                                             <td class="day_label">Drink :</td>
                                             <td class="day_value closed"><span><?php echo $memInfo->memDrink ?></span></td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </div>
                                 <div class="col-md-6 basic_1-left">
                                    <table class="table_working_hours">
                                       <tbody>
                                          <tr class="opened_1">
                                             <td class="day_label">Age :</td>
                                             <td class="day_value"><?php echo $memInfo->memAge ?></td>
                                          </tr>
                                          <tr class="opened">
                                             <td class="day_label">Mother Tongue :</td>
                                             <td class="day_value"><?php echo $memInfo->memTongue ?></td>
                                          </tr>
                                          <tr class="opened">
                                             <td class="day_label">Complexion :</td>
                                             <td class="day_value"><?php echo $memInfo->memComplexion ?></td>
                                          </tr>
                                          <tr class="opened">
                                             <td class="day_label">Weight :</td>
                                             <td class="day_value"><?php echo $memInfo->memWeight ?></td>
                                          </tr>
                                          <tr class="opened">
                                             <td class="day_label">Blood Group :</td>
                                             <td class="day_value"><?php echo $memInfo->memBlood ?></td>
                                          </tr>
                                          <tr class="closed">
                                             <td class="day_label">Diet :</td>
                                             <td class="day_value closed"><span><?php echo $memInfo->memDiet ?></span></td>
                                          </tr>
                                          <tr class="closed">
                                             <td class="day_label">Smoke :</td>
                                             <td class="day_value closed"><span><?php echo $memInfo->memSmoke ?></span></td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </div>
                                 <div class="clearfix"> </div>
                              </div>
                              <div class="basic_1">
                                 <h3>Religious / Social & Astro Background</h3>
                                 <div class="col-md-6 basic_1-left">
                                    <table class="table_working_hours">
                                       <tbody>
                                          <tr class="opened">
                                             <td class="day_label">Time of Birth :</td>
                                             <td class="day_value"><?php echo $memInfo->memBirthTime ?></td>
                                          </tr>
                                          <tr class="opened">
                                             <td class="day_label">Caste :</td>
                                             <td class="day_value"><?php echo $memInfo->memCaste ?></td>
                                          </tr>
                                          <tr class="opened">
                                             <td class="day_label">Date of Birth :</td>
                                             <td class="day_value"><?php echo $memInfo->memAgeDate ?>/<?php echo $memInfo->memAgeMonth ?>/<?php echo $memInfo->memAgeYear ?></td>
                                          </tr>
                                          <tr class="opened">
                                             <td class="day_label">City/Location :</td>
                                             <td class="day_value closed"><span><?php echo $memInfo->memBirthPlace ?></span></td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </div>
                                 <div class="col-md-6 basic_1-left">
                                    <table class="table_working_hours">
                                       <tbody>
                                          <tr class="opened_1">
                                             <td class="day_label">Religion :</td>
                                             <td class="day_value"><?php echo $memInfo->memReligion  ?></td>
                                             <!-- echo date(001) -->
                                          </tr>
                                          <tr class="opened">
                                             <td class="day_label">Sub Caste :</td>
                                             <td class="day_value"><?php echo $memInfo->memSubCaste ?></td>
                                          </tr>
                                          <tr class="opened">
                                             <td class="day_label">Raasi :</td>
                                             <td class="day_value"><?php echo $memInfo->memRaasi ?></td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </div>
                                 <div class="clearfix"> </div>
                              </div>
                              <div class="basic_1 basic_2">
                                 <h3>Education & Career</h3>
                                 <div class="basic_1-left">
                                    <table class="table_working_hours">
                                       <tbody>
                                          <tr class="opened">
                                             <td class="day_label">Education   :</td>
                                             <td class="day_value"><?php echo $memInfo->memEdu ?></td>
                                          </tr>
                                          <tr class="opened">
                                             <td class="day_label">Education Detail :</td>
                                             <td class="day_value"><?php echo $memInfo->memEduDetail ?></td>
                                          </tr>
                                          <tr class="opened">
                                             <td class="day_label">Occupation Detail :</td>
                                             <td class="day_value closed"><span><?php echo $memInfo->memOccDetail ?></span></td>
                                          </tr>
                                          <tr class="opened">
                                             <td class="day_label">Annual Income :</td>
                                             <td class="day_value closed"><span><?php echo $memInfo->memAnnualIncome ?></span></td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </div>
                                 <div class="clearfix"> </div>
                              </div>
                           </div>
                           <div role="tabpanel" class="tab-pane fade" id="profile" aria-labelledby="profile-tab">
                              <div class="basic_3">
                                 <h4>Family Details</h4>
                                 <div class="basic_1 basic_2">
                                    <h3>Basics</h3>
                                    <div class="col-md-6 basic_1-left">
                                       <table class="table_working_hours">
                                          <tbody>
                                             <tr class="opened">
                                                <td class="day_label">Father's Occupation :</td>
                                                <td class="day_value"><?php echo $memInfo->memFaOcc ?></td>
                                             </tr>
                                             <tr class="opened">
                                                <td class="day_label">Mother's Occupation :</td>
                                                <td class="day_value"><?php echo $memInfo->memMaOcc ?></td>
                                             </tr>
                                             <tr class="opened">
                                                <td class="day_label">No. of Brothers :</td>
                                                <td class="day_value closed"><span><?php echo $memInfo->memBroNum ?></span></td>
                                             </tr>
                                             <tr class="opened">
                                                <td class="day_label">No. of Sisters :</td>
                                                <td class="day_value closed"><span><?php echo $memInfo->memSisNum ?></span></td>
                                             </tr>
                                          </tbody>
                                       </table>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div role="tabpanel" class="tab-pane fade" id="profile1" aria-labelledby="profile-tab1">
                              <div class="basic_1 basic_2">
                                 <div class="basic_1-left">
                                    <table class="table_working_hours">
                                       <tbody>
                                          <tr class="opened">
                                             <td class="day_label">Age   :</td>
                                             <td class="day_value"><?php echo $memInfo->memPartAge ?></td>
                                          </tr>
                                          <tr class="opened">
                                             <td class="day_label">Marital Status :</td>
                                             <td class="day_value"><?php echo $memInfo->memPartMarital ?></td>
                                          </tr>
                                          <tr class="opened">
                                             <td class="day_label">Body Type :</td>
                                             <td class="day_value closed"><span><?php echo $memInfo->memPartBody ?></span></td>
                                          </tr>
                                          <tr class="opened">
                                             <td class="day_label">Complexion :</td>
                                             <td class="day_value closed"><span><?php echo $memInfo->memPartComplexion ?></span></td>
                                          </tr>
                                          <tr class="opened">
                                             <td class="day_label">Height 5ft 9 in / 175cm :</td>
                                             <td class="day_value closed"><span><?php echo $memInfo->memPartHeight ?></span></td>
                                          </tr>
                                          <tr class="opened">
                                             <td class="day_label">Diet :</td>
                                             <td class="day_value closed"><span><?php echo $memInfo->memPartHeight ?></span></td>
                                             <!-- **** -->
                                          </tr>
                                          <tr class="opened">
                                             <td class="day_label">Kujadosham / Manglik :</td>
                                             <td class="day_value closed"><span><?php echo $memInfo->memPartManglik ?></span></td>
                                          </tr>
                                          <tr class="opened">
                                             <td class="day_label">Religion :</td>
                                             <td class="day_value closed"><span><?php echo $memInfo->memPartReligion ?></span></td>
                                          </tr>
                                          <tr class="opened">
                                             <td class="day_label">Caste :</td>
                                             <td class="day_value closed"><span><?php echo $memInfo->memPartCaste ?></span></td>
                                          </tr>
                                          <tr class="opened">
                                             <td class="day_label">Mother Tongue :</td>
                                             <td class="day_value closed"><span><?php echo $memInfo->memPartTongue ?></span></td>
                                          </tr>
                                          <tr class="opened">
                                             <td class="day_label">Education :</td>
                                             <td class="day_value closed"><span><?php echo $memInfo->memPartEducation ?></span></td>
                                          </tr>
                                          <tr class="opened">
                                             <td class="day_label">Occupation :</td>
                                             <td class="day_value closed"><span><?php echo $memInfo->memPartOccupation ?></span></td>
                                          </tr>
                                          <tr class="opened">
                                             <td class="day_label">Country of Residence :</td>
                                             <td class="day_value closed"><span><?php echo $memInfo->memPartCountry ?></span></td>
                                          </tr>
                                          <tr class="opened">
                                             <td class="day_label">State :</td>
                                             <td class="day_value closed"><span><?php echo $memInfo->memPartState ?></span></td>
                                          </tr>
                                          <tr class="opened">
                                             <td class="day_label">Residency Status :</td>
                                             <td class="day_value closed"><span><?php echo $memInfo->memPartResidency ?></span></td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                           <!-- message profile start -->
                           <?php $this->session->userData('memUname') ?>
                           <div role="tabpanel" class="tab-pane fade" id="profile4" aria-labelledby="profile-tab1">
                              <div class="basic_1 basic_2">
                                 <div class="basic_1-left">
                                    <table class="table_working_hours">
                                       <tbody>
                                          <tr class="opened">
                                             <form action="<?php echo site_url('members/messageAction');?>" method="post">

                                                <input type="hidden" name="redirectUrl" value="<?php echo current_url(); ?>" >

                                                <textarea style="width: 100%" rows="9" name="msgBox" placeholder="Write Your Message Here"></textarea>
                                                <br>

                                                <?php
                                                   $receiverId = $this->uri->segment(3); 

                                                   $memUname = $this->session->userData('memUname');
                                                   $q = $this->M_crud->find('members', array('memUname' => $memUname));
                                                   $senderId = $q->memId;
                                                   ?>
                                                <input type="hidden" name="senderId" value="<?php echo $senderId ?>" >
                                                <input type="hidden" name="receiverId" value="<?php echo $receiverId ?>" >
                                                <div class="form-actions">
                                                   <input type="submit" id="edit-submit" value="SEND" class="btn_1 submit">
                                                </div>
                                             </form>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                           <!-- message profile end -->
                         
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-4 profile_right">
                  <div class="members_box1">
                     <div class="newsletter">
                        <form action="member/searchAction.php" method="post">
                           <input type="text" name="search" size="30" required="" ">
                           <input type="submit" value="Go">
                        </form>
                     </div>
                     <div class="clearfix"> </div>
                  </div>
                  <div class="view_profile">
                     <h3>View Similar Profiles</h3>
                     <ul class="profile_item">
                     <?php foreach ($similler as $v):
                         ?>
                           <li>
                              <a href="<?php echo site_url('members/viewProfile/'.$v->memId) ?>">
                                 <li class="profile_item-img">
                                    <?php if (!empty($v->memImage1)): ?>
                                       <img src="<?php echo base_url('uploads/'.$v->memImage1) ?>" class="img-responsive" alt=""/>     
                                    <?php else: ?>
                                       <img src="<?php echo base_url('resource/front-end/Images/default.jpg') ?>" class="img-responsive" alt=""/>
                                    <?php endif; ?>
                                 </li>
                                 <li class="profile_item-desc">
                                    <h4><?php echo $v->memProId ?></h4>
                                    <p><?php echo $v->memAge ?> Yrs, <?php echo $v->memOccDetail ?>,<br><?php echo $v->memReligion ?>,<?php echo $v->memBirthPlace ?></p>
                                    <h5>View Full Profile</h5>
                                 </li>
                                 <div class="clearfix"> </div>
                              </a>
                           </li>
                           <br>
                        <?php endforeach; ?>  
                     </ul>
                  </div>
                  <div class="view_profile view_profile1">
                     <h3>Members who viewed this profile also viewed</h3>
                     <?php foreach ($suggest as $v ):?>
                        <ul class="profile_item">
                           <a href="<?php echo site_url('members/viewProfile/'.$v->memId) ?>">
                              <li class="profile_item-img">
                                 <?php if (!empty($v->memImage1)): ?>
                                    <img src="<?php echo base_url('uploads/'.$v->memImage1) ?>" class="img-responsive" alt=""/>    
                                 <?php else: ?>
                                    <img src="<?php echo base_url('resource/front-end/Images/default.jpg') ?>" class="img-responsive" alt=""/>
                                 <?php endif; ?>
                              </li>
                              <li class="profile_item-desc">
                                 <h4><?php echo $v->memProId ?></h4>
                                 <p><?php echo $v->memAge ?> Yrs, <?php echo $v->memOccDetail ?>,<br><?php echo $v->memReligion ?>,<?php echo $v->memBirthPlace ?></p>
                                 <h5>View Full Profile</h5>
                              </li>
                              <div class="clearfix"> </div>
                           </a>
                        </ul>
                     <?php endforeach; ?>   
                  </div>
               </div>
               <div class="clearfix"> </div>
            </div>
         </div>
      </div>
      <?php $this->load->view('marital/footer') ?>
      <!-- FlexSlider -->
      <script defer src="<?php echo base_url('resource/front-end/js/jquery.flexslider.js') ?>"></script>
      <link rel="stylesheet" href="<?php echo base_url('resource/front-end/css/flexslider.css') ?>" type="text/css" media="screen" />
      <script>
         // Can also be used with $(document).ready()
         $(window).load(function() {
           $('.flexslider').flexslider({
             animation: "slide",
             controlNav: "thumbnails"
           });
         });
      </script>
   </body>
</html>

