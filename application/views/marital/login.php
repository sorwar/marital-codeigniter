

<!DOCTYPE HTML>
<html>
   <head>
      <?php $this->load->view('marital/head') ?>
   <body>
      <!-- ============================  Navigation Start =========================== -->
      <?php $this->load->view('marital/header') ?>
      <!-- ============================  Navigation End ============================ -->
      <div class="grid_3">
         <div class="container">
            <div class="breadcrumb1">
               <ul>
                  <a href="index.html"><i class="fa fa-home home_1"></i></a>
                  <span class="divider">&nbsp;|&nbsp;</span>
                  <li class="current-page">Login</li>
               </ul>
            </div>
            <div class="services">
               <div class="col-sm-6 login_left">
                  <?php
                     $failed = $this->uri->segment(3);
                        if ($failed == 'failed'):        
                     ?>
                     <div class="alert alert-danger">
                        <strong>Username or Password is Incorrect!</strong>
                     </div>
                  <?php elseif ($failed == 'login-first'): ?>
                     <div class="alert alert-danger">
                        <strong>You have to login first!</strong>
                     </div>
                  <?php elseif ($failed == 'deactivated'): ?>
                     <div class="alert alert-danger">
                        <strong>Your account has been deactivated !</strong>
                     </div>
                  <?php elseif ($failed == 'not-active'): ?>
                     <div class="alert alert-danger">
                        <strong>Your account isn't active yet !</strong>
                     </div>
                  <?php endif ?>
                  <form action="<?php echo site_url('home/loginAction') ?>" method="post">
                     <div class="form-item form-type-textfield form-item-name">
                        <label for="edit-name">Username <span class="form-required" title="This field is required.">*</span></label>
                        <input type="text" id="edit-name" name="memUname" value="" size="60" maxlength="60" class="form-text required">
                     </div>
                     <div class="form-item form-type-password form-item-pass">
                        <label for="edit-pass">Password <span class="form-required" title="This field is required.">*</span></label>
                        <input type="password" id="edit-pass" name="memPass" size="60" maxlength="128" class="form-text required">
                     </div>
                     <div class="form-actions">
                        <input type="submit" id="edit-submit" name="op" value="Log in" class="btn_1 submit">
                     </div>
                  </form>
                  <br>
                  <br>
                  <li style="position: relative; left: 15px">Don't Have a Account? <a style="color:  #d80843  " href="<?php echo site_url('home/register') ?>">Regester Now</a></li>
               </div>
               <div class="col-sm-6">
                  <ul class="sharing">
                     <li><a href="#" class="facebook" title="Facebook"><i class="fa fa-boxed fa-fw fa-facebook"></i> Share on Facebook</a></li>
                     <li><a href="#" class="twitter" title="Twitter"><i class="fa fa-boxed fa-fw fa-twitter"></i> Tweet</a></li>
                     <li><a href="#" class="google" title="Google"><i class="fa fa-boxed fa-fw fa-google-plus"></i> Share on Google+</a></li>
                     <li><a href="#" class="linkedin" title="Linkedin"><i class="fa fa-boxed fa-fw fa-linkedin"></i> Share on LinkedIn</a></li>
                     <li><a href="#" class="mail" title="Email"><i class="fa fa-boxed fa-fw fa-envelope-o"></i> E-mail</a></li>
                  </ul>
               </div>
               <div class="clearfix"> </div>
            </div>
         </div>
      </div>
      <?php $this->load->view('marital/footer') ?>
   </body>
</html>

