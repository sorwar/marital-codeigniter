<!DOCTYPE HTML>
<html>
	<?php $this->load->view('marital/head') ?>
   <body>
      <!-- ============================  Navigation Start =========================== -->
      <?php $this->load->view('marital/header') ?>
      <!-- end navbar-inverse-blue -->
      <!-- ============================  Navigation End ============================ -->
      <div class="banner"  style="background:url(<?php echo base_url('uploads/'.$basicInfo->background ) ?>) no-repeat center top; background-size:cover; -webkit-background-size:cover; -moz-background-size:cover; -o-background-size:cover;">
         <div class="container" style="width: 100%" >
            <div class="row">
               <div class="col-md-1">
               </div>
               <div class="banner_info  col-md-9" style="margin-bottom: 20px"">
                  <h3>Millions of verified Members</h3>
                  <?php 
                     $memUname = $this->session->userData('memUname');
                   if (empty($memUname)): ?>
                     <a href="<?php echo site_url('home/register') ?>" class="hvr-shutter-out-horizontal">Create Your Profile</a>
                     <?php else:
                           $q      = $this->M_crud->find('members', array('status' => '1', 'memUname' => $memUname));
                           $memId  = $q->memId;
                        
                      ?>
                     <a href="<?php echo site_url('members/viewProfile/'.$memId) ?>" class="hvr-shutter-out-horizontal">Visit Your Profile</a>
                  <?php endif; ?>
               </div>
            </div>
         </div>
         <div class="profile_search">
            <div class="container wrap_1">
               <form action="<?php echo site_url('home/searchAction') ?>" method="post"">
                  <div class="search_top">
                     <div class="inline-block">
                        <label class="gender_1">I am looking for :</label>
                        <div class="age_box1" style="max-width: 100%; display: inline-block;">
                           <select name="memSex" >
                              <option value="">* Select Gender</option>
                              <option value="Bride">Bride</option>
                              <option value="Groom">Groom</option>
                           </select>
                        </div>
                     </div>
                     <div class="inline-block">
                        <label class="gender_1">Located In :</label>
                        <div class="age_box1" style="max-width: 100%; display: inline-block;">
                           <select name="memBirthPlace" >
                              <option value="">* Select State</option>
                              <option value="Washington">dhaka</option>
                              <option value="Texas">rajshahi</option>
                              <option value="Georgia">Georgia</option>
                              <option value="Virginia">Virginia</option>
                              <option value="Colorado">Colorado</option>
                           </select>
                        </div>
                     </div>
                     <div class="inline-block">
                        <label class="gender_1">Height :</label>
                        <div class="age_box1" style="max-width: 100%; display: inline-block;">
                           <select name="memHeight">
                              <option value="">* Select Height</option>
                              <option value="5ft 1 in">5 ft 1 in</option>
                              <option value="5ft 2 in">5 ft 2 in</option>
                              <option value="5ft 3 in">5 ft 3 in</option>
                              <option value="5ft 4 in">5 ft 4 in</option>
                              <option value="5ft 5 in">5 ft 6 in</option>
                           </select>
                        </div>
                     </div>
                  </div>
                  <div class="inline-block">
                        <label class="gender_1">Age :</label>
                        <div class="age_box1" style="max-width: 34%; display: inline-block;">
                           <select name="fromAge">
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                    <option value="31">31</option>
                                    <option value="32">32</option>
                                    <option value="33">33</option>
                                    <option value="34">34</option>
                                    <option value="35">35</option>
                                    <option value="36">36</option>
                                    <option value="37">37</option>
                                    <option value="38">38</option>
                                    <option value="39">39</option>
                                    <option value="40">40</option>
                                    <option value="41">41</option>
                                    <option value="42">42</option>
                                    <option value="43">43</option>
                                    <option value="44">44</option>
                                    <option value="45">45</option>
                                    <option value="46">46</option>
                                    <option value="47">47</option>
                                    <option value="48">48</option>
                                    <option value="49">49</option>
                                    <option value="50">50</option>
                                    <option value="51">51</option>
                                    <option value="52">52</option>
                                    <option value="53">53</option>
                                    <option value="54">54</option>
                                    <option value="55">55</option>
                                    <option value="56">56</option>
                                    <option value="57">57</option>
                                    <option value="58">58</option>
                                    <option value="59">59</option>
                                    <option value="60">60</option>
                           </select>
                        </div>
                        <label class="gender_1">TO :</label>
                        <div class="age_box1" style="max-width: 34%; display: inline-block;">
                           <select name="toAge">
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                    <option selected="" value="31">31</option>
                                    <option value="32">32</option>
                                    <option value="33">33</option>
                                    <option value="34">34</option>
                                    <option value="35">35</option>
                                    <option value="36">36</option>
                                    <option value="37">37</option>
                                    <option value="38">38</option>
                                    <option value="39">39</option>
                                    <option value="40">40</option>
                                    <option value="41">41</option>
                                    <option value="42">42</option>
                                    <option value="43">43</option>
                                    <option value="44">44</option>
                                    <option value="45">45</option>
                                    <option value="46">46</option>
                                    <option value="47">47</option>
                                    <option value="48">48</option>
                                    <option value="49">49</option>
                                    <option value="50">50</option>
                                    <option value="51">51</option>
                                    <option value="52">52</option>
                                    <option value="53">53</option>
                                    <option value="54">54</option>
                                    <option value="55">55</option>
                                    <option value="56">56</option>
                                    <option value="57">57</option>
                                    <option value="58">58</option>
                                    <option value="59">59</option>
                                    <option value="60">60</option>
                           </select>
                        </div>
                     </div>
                  <div class="inline-block">
                     <label class="gender_1">Status :</label>
                     <div class="age_box1" style="max-width: 100%; display: inline-block;">
                        <select name="memMarital">
                           <option value="Single">Single</option>
                           <option value="Divorced">Divorced</option>
                           <option value="Separated">Separated</option>
                           <option value="Other">Other</option>
                        </select>
                     </div>
                  </div>
                  <div class="submit inline-block">
                     <input id="submit-btn" class="hvr-wobble-vertical" type="submit" value="Find Matches">
                  </div>
               </form>
            </div>
         </div>
      </div>
      <div class="grid_1">
         <div class="container">
            <h1>Featured Profiles</h1>
            <div class="heart-divider">
               <span class="grey-line"></span>
               <i class="fa fa-heart pink-heart"></i>
               <i class="fa fa-heart grey-heart"></i>
               <span class="grey-line"></span>
            </div>
            <ul id="flexiselDemo3">
            <?php foreach ($memberInfo as $v) :  ?>
               <li>
                  <div class="col_1">
                     <a href="<?php echo site_url('members/viewProfile/'.$v->memId) ?>">
                        <img style="height: 100px;" src="<?php echo base_url('uploads/'.$v->memImage1) ?>" alt="" class="hover-animation image-zoom-in img-responsive"/>
                        <div class="layer m_1 hidden-link hover-animation delay1 fade-in">
                           <div class="center-middle">About Him</div>
                        </div>
                        <h3><span class="m_3">Profile ID : <?php echo $v->memProId?></span><br><?php echo $v->memAge?>, <?php echo $v->memBirthPlace?><br><?php echo $v->memReligion?></h3>
                     </a>
                  </div>
               </li>
            <?php endforeach ?>
            </ul>
            <script type="text/javascript">
               $(window).load(function() {
               $("#flexiselDemo3").flexisel({
               	visibleItems: 6,
               	animationSpeed: 1000,
               	autoPlay:false,
               	autoPlaySpeed: 3000,    		
               	pauseOnHover: true,
               	enableResponsiveBreakpoints: true,
                  	responsiveBreakpoints: { 
                  		portrait: { 
                  			changePoint:480,
                  			visibleItems: 1
                  		}, 
                  		landscape: { 
                  			changePoint:640,
                  			visibleItems: 2
                  		},
                  		tablet: { 
                  			changePoint:768,
                  			visibleItems: 3
                  		}
                  	}
                  });
                  
               });
                
            </script>
            <script type="text/javascript" src="<?php echo base_url('resource/front-end/js/jquery.flexisel.js') ?>"></script>
         </div>
      </div>
      <?php $this->load->view('marital/footer') ?>

   </body>
</html>
