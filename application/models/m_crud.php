<?php

	class M_crud extends CI_Model {
	
		public function __construct()
		{
			parent::__construct();
		}

		public function save($table, $data)
		{
			$this->db->insert($table, $data);      
			return $this->db->insert_id();  
		}		
		
		public function find($table, $where = array(), $order = "")
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where($where);
			if(!empty($order)) $this->db->order_by($order);
			$query = $this->db->get();
			return $query->row();
		}

		public function countAll($table, $where = array())
	    {
	        $this->db->select('*');
	        $this->db->from($table);
		    $this->db->where($where);
			$query = $this->db->count_all_results();
	        return $query;
	    }	

		public function findAll($table, $where = array(), $order = "", $onset = 0, $limit = 4)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where($where);
			if(!empty($order)) $this->db->order_by($order);
			if(!empty($limit)) $this->db->limit($limit, $onset);
			$query = $this->db->get();
			return $query->result();
		}	

		public function findAllPagination($table, $where = array(), $onset = 0)
		{
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where($where);
			$this->db->limit(4, $onset);
			$query = $this->db->get();
			return $query->result();
		}	
		
		public function update($table, $data, $where)
		{
			$this->db->update($table, $data, $where);        
		}
		
		public function destroy($table, $where)
		{
			$this->db->delete($table, $where);       
		}

		public function get_menu()
		{
		    $catagory_list = $this->findAll("catagory");

		    foreach ($catagory_list as $v) {
		    	$cat_id = $v->cat_id;
		    	$sub_cat_list = $this->findAll("subcat", array("parant_cat" => $cat_id));

		    	foreach ($sub_cat_list as $sub) {
		    		$subcat_id = $sub->subcat_id;
		    		$child_cat_list = $this->findAll("childcat", array("parent_sub_catagory" => $subcat_id));
		    		
		    		$sub->child_cat_list = $child_cat_list;
		    	}
		    	
		    	$v->sub_cat_list = $sub_cat_list;
		    }

		    return $catagory_list;
		}
			
	}

