<?php

	class Custom_model extends CI_Model {
	
		public function __construct()
		{
			parent::__construct();
		}

		public function inboxJoin($receiverId)
		{
			$this->db->select('messages.*, members.memUname, members.memImage1');
			$this->db->from('messages');
			$this->db->where(array('msgReceiver' => $receiverId));
			$this->db->order_by('msgId desc');
			$this->db->join('members', 'members.memId = messages.msgSender', 'left');
			$query = $this->db->get();
			return $query->result();
		}

		public function sentJoin($semderId)
		{
			$this->db->select('messages.*, members.memUname, members.memImage1');
			$this->db->from('messages');
			$this->db->where(array('msgSender' => $semderId));
			$this->db->order_by('msgId desc');
			$this->db->join('members', 'members.memId = messages.msgReceiver', 'left');
			$query = $this->db->get();
			return $query->result();
		}

		public function adminMemJoin()
		{
			$this->db->select('messages.*, memReceiver.memUname as receiverUname, memReceiver.memImage1 as recieverImage, memSender.memUname as senderUname, memSender.memImage1 as senderImage');
			$this->db->from('messages');
			$this->db->order_by('msgId desc');
			$this->db->join('members as memReceiver', 'memReceiver.memId = messages.msgReceiver', 'left');
			$this->db->join('members as memSender', 'memSender.memId = messages.msgSender', 'left');
			$query = $this->db->get();
			return $query->result();
		}
			
	}

